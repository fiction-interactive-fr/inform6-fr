Début d'une transcription de...

>replay
Do you want MORE prompts? (y/n) >n
[Rejouer les commandes.]

>prendre paille
Vous sélectionnez délicatement une des pailles les plus rigides dans une
couchette (pas dans la vôtre bien évidemment, vous tenez à votre confort), et
vous en emparez d'un geste.

>o
"Te voilà enfin, mon cher Chevalier. Je t'ai fait quérir car je dois te faire
part d'une requête importante..."

Le roi s'arrête un instant, refoulant un éternuement, et poursuit son discours,
non sans bégayer.

"Peut-être l'as-tu remarqué, le blason de la famille royale nous a été volé. La
bande de Kay, les rats qui vivent dans la cave de la maison, a déjà revendiqué
le crime, bien que j'aie toujours du mal à imaginer comment ils ont pu procéder,
étant trop grands pour pénétrer notre palais.
Toujours est-il que..."

Le roi s'interrompt à nouveau, toussant violemment si bien que vous n'avez
d'autre recours que d'aller lui tapoter gentiment le dos de votre patte agile
jusqu'à ce qu'il retrouve une respiration correcte.

"Hu... Excuse-moi... Bref, je disais que j'ai besoin de toi pour récupérer cet
artefact de valeur inestimable, rongé dans une plaque de PVC par mon arrière-
grand-oncle, et gravé par sa sœur, artiste de renom.
Voici tes ordres de mission. J'ai confiance en toi, mon meilleur Chevalier. Je
sais que tu ne me décevras pas. Maintenant, va !"

Palais du roi Mice
Vous êtes devant le trône de votre roi, Mice VII, dressé dans une cavité creusée
dans le béton par les pattes de vos ancêtres, derrière le vieux buffet du salon.
Le trône, somptueux, est taillé dans un bloc de polystyrène dans lequel deux
pieux, ou cure-dents, tiennent lieu d'accoudoirs. Au-dessus du trône se trouve
un emplacement circulaire qui accueille en principe les armes de la famille
royale, mais qui est vide aujourd'hui. Le roi, confortablement lové au creux de
son siège, promène un regard bienveillant sur ses sujets affairés autour de lui,
tandis qu'il grignote un morceau de fromage en plastique rigide. De l'entrée en
arcade du palais provient une faible lumière qui est ingénieusement répartie
dans la salle à l'aide de bris d'un miroir. La porte est au nord, tandis qu'à
l'est s'étendent les dortoirs.

Vous pouvez voir des sujets de la cour et le roi Mice VII.

>n

Sous le vieux buffet
Vous voilà devant l'entrée du palais royal, contre le mur sud du salon, sous le
vieux buffet qui siège entre l'ordinateur à l'est et le téléviseur à l'ouest. Au
nord vous êtes au beau milieu de la pièce, à découvert, et à la merci de
Choupinette.

>poser paille
D'accord.

>grimper
(la paille de plastique)
Vous agrandissez la paille rétractable en accordéon et l'installez en oblique à
travers le trou, puis entreprenez de l'escalader jusqu'en son sommet.

Dans le vieux buffet
À l'intérieur du buffet en acajou, la seule lumière qui filtre provient du trou
par lequel vous êtes arrivé et des pourtours des portes de bois. Le sol
poussiéreux est marqué de traces de pattes de souris vers un angle. La sortie
est vers le bas.

Vous pouvez voir un foulard noir.

>prendre foulard
D'accord.

[Votre score vient d'augmenter de un point.]

>mettre foulard
Vous mettez le foulard noir.

>descendre
Vous glissez avec précaution le long de la paille.

Sous le vieux buffet
Vous voilà devant l'entrée du palais royal, contre le mur sud du salon, sous le
vieux buffet qui siège entre l'ordinateur à l'est et le téléviseur à l'ouest. Au
nord vous êtes au beau milieu de la pièce, à découvert, et à la merci de
Choupinette.

Vous pouvez voir une paille de plastique.

>o

Près du poste de télévision
À l'ouest du buffet où se trouve l'entrée du palais royal siège fièrement un
énorme téléviseur à écran cathodique, qui a longtemps été votre terrain de jeu
lorsque vous n'étiez qu'un souriceau et que vous pouviez vous y faufiler.
Aujourd'hui vous êtes trop grand pour y entrer, et vous n'avez plus votre
insouciance de jeunesse, préférant désormais éviter tout contact avec un
matériel électrique. Au nord vous pouvez vous glisser derrière le canapé
installé devant l'écran et rejoindre le balcon, au nord-est vous êtes au centre
du salon et à l'est vous retournez sous le buffet.

Vous pouvez voir la fille.

>n

Sur le balcon
La porte du balcon entrouverte vous permet de vous y faufiler, et une légère
brise vient caresser vos moustaches frémissantes. Ce lieu semble être une annexe
du garage pour les propriétaires du lieu tant il est encombré de bicyclettes, de
pots de peinture et balconnières garnies de fleurs. Au sud vous retrouvez le
canapé ainsi que le téléviseur tandis qu'à l'est vous traversez le salon à
découvert.

>grimper plante

Au sommet de l'arbuste
Vous êtes au sommet d'un arbuste qui surplombe un pot de peinture noire dans
lequel se jette une de ses longues feuilles pendantes, et vous pouvez
redescendre sur le balcon par une de ses branches qui chemine vers le sol.

>sauter
Pris soudain d'une envie irrépressible, vous bouchez vos narines à l'aide d'une
de vos pattes et plongez dans le pot de peinture noire. Vous en ressortez tout
peint en grimpant le long de la feuille et sautez sur le sol du balcon,
désormais invisible dans l'obscurité.

Sur le balcon
La porte du balcon entrouverte vous permet de vous y faufiler, et une légère
brise vient caresser vos moustaches frémissantes. Ce lieu semble être une annexe
du garage pour les propriétaires du lieu tant il est encombré de bicyclettes, de
pots de peinture et balconnières garnies de fleurs. Au sud vous retrouvez le
canapé ainsi que le téléviseur tandis qu'à l'est vous traversez le salon à
découvert.

[Votre score vient d'augmenter de trois points.]

>s

Près du poste de télévision
À l'ouest du buffet où se trouve l'entrée du palais royal siège fièrement un
énorme téléviseur à écran cathodique. Au nord vous pouvez vous glisser derrière
le canapé installé devant l'écran et rejoindre le balcon, au nord-est vous êtes
au centre du salon et à l'est vous retournez sous le buffet.

Vous pouvez voir la fille.

>e

Sous le vieux buffet
Vous voilà devant l'entrée du palais royal, contre le mur sud du salon, sous le
vieux buffet qui siège entre l'ordinateur à l'est et le téléviseur à l'ouest. Au
nord vous êtes au beau milieu de la pièce, à découvert, et à la merci de
Choupinette.

Vous pouvez voir une paille de plastique.

>e

Sous le bureau de l'ordinateur
Vous vous glissez sous le bureau de l'ordinateur familial, un énorme engin au
ronronnement inquiétant qui résonne de mille clics sous l'action effrénée du
fils de la maison, scotché devant un écran d'une taille démesurée, casque sur
les oreilles, et gesticulant visiblement aux prises avec des terroristes
virtuels. À l'ouest vous retrouvez le calme de votre buffet, le téléviseur et
plus loin le balcon, au nord-ouest s'ouvre toujours un espace vide et
étrangement silencieux et à l'est se trouve l'entrée d'un couloir.

Vous pouvez voir le fils.

>eteindre ordinateur
Vous promenez une patte discrète sur les câblages de la machine et suite à une
maladresse, débranchez un des fils. Un hurlement de rage retentit et le jeune
garçon se lève d'un bond, proférant jurons et injures. Il lui faut plusieurs
minutes pour découvrir la source de la coupure, et vous avez l'occasion de voir
son visage bouffi s'empourprer quand son sang lui monte au cerveau alors qu'il
est penché pour rebrancher le câble, et ressortir grisâtre, poussiéreux.

Forcé de s'arrêter par le redémarrage de l'ordinateur, le jeune homme en profite
pour s'éclipser aux toilettes, dont il laisse la porte entrouverte en sortant.

[Votre score vient d'augmenter de un point.]

>e

Couloir vers la cuisine
L'unique couloir de la maison qui joint, du nord au sud, la cuisine et les
chambres. À l'est se trouvent la salle de bain dont la porte est fermée, et les
toilettes, dont l'accès vous est désormais possible.

>e

Dans les toilettes
Vous vous trouvez dans les toilettes de la propriété, attenantes à la salle de
bain, où le trône prend tout de suite une dimension autre que celle du trône du
roi Mice VII. À côté de vous se trouve une brosse sur le manche de laquelle sont
enfilés plusieurs rouleaux de papier-toilette blanc immaculé. À l'ouest vous
retournez dans le couloir.

>prendre papier
Vous poussez de votre patte l'un des rouleaux, ce qui à pour effet de faire
balloter le manche de la brosse tant et si bien que le rouleau situé tout en
haut finit par vous tomber dessus, et en vous en extirpant vous vous empêtrez
dans les feuilles de papier qui vous recouvrent presque entièrement.

[Votre score vient d'augmenter de deux points.]

>o

Couloir vers la cuisine
L'unique couloir de la maison qui joint, du nord au sud, la cuisine et les
chambres. À l'est se trouvent la salle de bain dont la porte est fermée, et les
toilettes, dont l'accès vous est désormais possible.

>s

Couloir vers les chambres
Le couloir qui rejoint la cuisine et le salon au nord, donne ici sur trois
chambres : celle du fils à l'ouest, de la fille au sud et des parents à l'est,
cette dernière étant la seule porte ouverte par ailleurs.

>e

Chambre des parents
La chambre des propriétaires de la maison est des plus sommaires, contenant un
lit deux places, un bureau, une commode et une armoire entrouverte. Une fenêtre,
ouverte, donne sur le jardin et la mère y est accoudée, épuisant une cigarette.

Vous pouvez voir la mère.

>r morceau
Un bout de parchemin que vous devriez être en mesure de vous approprier.

>prendre morceau
Vous prenez un morceau de parchemin qui était coincé sous la porte droite de
l'armoire.

[Votre score vient d'augmenter de un point.]

>o

Couloir vers les chambres
Le couloir qui rejoint la cuisine et le salon au nord, donne ici sur trois
chambres : celle du fils à l'ouest, de la fille au sud et des parents à l'est,
cette dernière étant la seule porte ouverte par ailleurs.

>n

Couloir vers la cuisine
L'unique couloir de la maison qui joint, du nord au sud, la cuisine et les
chambres. À l'est se trouvent la salle de bain dont la porte est fermée, et les
toilettes, dont l'accès vous est désormais possible.

>n

La cuisine
Relativement exiguë, la cuisine est plutôt calme, les enfants étant plongés dans
leurs activités numériques tandis que la mère se trouve dans sa chambre. Du
robinet au-dessus de l'évier goutte à intervalle régulier une petite note d'eau
qui vient résonner de son ploc dans l'atmosphère moite de la pièce. Au sud se
trouve le couloir menant au salon et aux chambres, tandis qu'à l'ouest un
escalier descend vers la cave et qu'à l'est se tient la porte d'entrée de la
maison.

>n
Vous ne pouvez pas aller par là.

>a
Le temps passe...

>a
Le temps passe...

>a
Le temps passe...

La porte s'ouvre brusquement sur le maître de maison, rentrant du jardin où il
cultivait son potager. Il se dirige vers la cave, ouvrant la porte, s'y déleste
de son matériel de jardinage et part retrouver sa femme, laissant la porte
grande ouverte à l'ouest.

>o

Au plus profond de la cave
La cave de la maison est extrêmement petite, et ne sert qu'à entreposer le vin,
les outils et vêtements de jardin du père. L'obscurité est quasi totale puisque
seul un soupirail vient éclairer le lieu, mais vous voyez néanmoins l'entrée du
repaire de Kay sur le mur nord. L'escalier remonte à la cuisine à l'est.

>n
Gauche et veule, vous avancez néanmoins au devant des gardes du trou qui sont
saisis d'effroi à la vue de votre accoutrement.

"Un fantôme ! Un fantôme !" s'écrie le premier, terrifié, tandis que le second
s'enfuit en vous frôlant.

Amusé, vous pouvez désormais pénétrer dans l'enceinte du repaire du gang du rat
Kay.

Hall du repaire de Kay
L'entrée de l'antre de Kay s'enfonce de plusieurs centimètres et passe sous le
mur de la maison. Les relents de rats crevés qui stagnent dans l'air vous
donnent d'imaginer ce qu'il pourrait advenir de vous s'il vous mettaient la
patte dessus. Sur les murs sont installées des diodes de récupération branchées
à une dynamo, au moins grosse comme vous, qu'actionne un vieux rat difforme, et
aveugle semble-t-il. Mais loin d'être pessimiste vous préférez analyser
froidement la situation : selon les pancartes, à l'ouest seraient les quartiers
des membres du gang, à l'est les prisons et l'entrepôt, et au nord la salle de
réunion où se tient probablement le rat Kay lui-même. Au sud, la lumière, et la
sortie.

[Votre score vient d'augmenter de huit points.]

>o

Couloir des dortoirs
Des bannières pleines de sang séché pendent à des tringles rouillées, marquant
l'entrée des quartiers des membres du gang de Kay. Au nord et au sud, deux
passages mènent à deux dortoirs. À l'est, vous retournez vers le hall.

>s

Dortoir sud
Ce dortoir est austère, et un calme plat y règne. La lumière y est aussi
importante, mais personne ne semble là pour vous y identifier. Des lits
superposés taillés dans de vieux tiroirs de bois sont alignés sur deux rangées,
et quelques boîtes d'allumettes vides viennent jouer le rôle de coffres
personnels.

>fouiller coffre
Une boîte de 240 allumettes attire votre attention car elle est entrouverte et
un objet fin y reluit. En vous approchant vous découvrez un fin poignard d'acier
trempé dont la garde dépasse de la boîte.

>prendre poignard
À peine avez-vous serré vos doigts autour de la garde du poignard qu'une patte
imposante vient se poser sur votre épaule droite.

"Quel est cet accoutrement que tu portes, Stanley ?"

Stanley... Ce nom ne vous est pas inconnu. Vous paniquez quelque peu en voyant
le rat qui vient de vous surprendre, qui devait être assis dans un angle quand
vous avez inspecté la salle. En croisant son regard inquisiteur vous pensez
qu'il vaut mieux coopérer jusqu'à ce qu'une occasion se présente d'elle-même.
Vous vous dépêtrez non sans mal de l'amas de feuilles de papier qui vous
recouvre et retirez le foulard noir qui vous masque le museau, tandis que le
visage du rat s'éclaire.

"Un moment j'ai eu un doute mais je vois que c'est bien toi Stan !" Et l'autre
de s'esclaffer bêtement. "C'est vrai que des petiots comme toi y-en a pas des
masses. Fallait bien ça pour le blason, hein ?"
Le rat vous tapote l'épaule et repart, rieur.

[Votre score vient d'augmenter de un point.]

>n
Stanley... Qu'est-ce que Stanley a à voir avec cette intrigue ? Pourquoi est-ce
que son poignard se trouve dans le repaire de Kay ?

Stanley était un de vos amis d'enfance. Le roi actuel, lui et vous formiez un
trio inséparable, mais il y a peu Stanley a disparu sans laisser de nouvelles.
Serait-ce possible que... que Stanley soit véritablement le voleur ?

Couloir des dortoirs
Des bannières pleines de sang séché pendent à des tringles rouillées, marquant
l'entrée des quartiers des membres du gang de Kay. Au nord et au sud, deux
passages mènent à deux dortoirs. À l'est, vous retournez vers le hall.

>e

Hall du repaire de Kay
L'entrée de l'antre de Kay s'enfonce de plusieurs centimètres et passe sous le
mur de la maison. Une dynamo, au moins grosse comme vous, est actionnée par un
vieux rat difforme et aveugle. Selon les pancartes, à l'ouest seraient les
quartiers des membres du gang, à l'est les prisons et l'entrepôt, et au nord la
salle de réunion où se tient probablement le rat Kay lui-même. Au sud, la
lumière, et la sortie.

>e

Couloir des prisons
Au nord, au sud, quatre prisons, dans deux desquelles se morfondent deux rats,
l'un blanc, sûrement isolé car il était albinos, l'autre plus classique,
probablement un opposant politique ou un traître. Toujours est-il qu'aucun
n'ouvre la bouche. L'une des deux portes au nord est ouverte et donne sur une
geôle vide. Le couloir chemine d'est en ouest, vers le hall à l'ouest et
l'entrepôt à l'est.

>n
Vous avez la désagréable impression que ce lieu sera pour longtemps votre
résidence et songez à sortir.

Dans une geôle vide
Des murs griffés par les générations de prisonniers qui s'y sont succédé, cette
geôle vous apparaît lugubre, dans sa sinistre robe grisâtre. Des barreaux
verticaux, serrés et raides, forment la façade sud dans laquelle s'ouvre la
porte, de barreaux elle aussi, par laquelle vous êtes entré.

Vous pouvez voir une boîte de conserve vide.

>prendre boite
D'accord.

>s

Couloir des prisons
Au nord, au sud, quatre prisons, dans deux desquelles se morfondent deux rats,
l'un blanc, sûrement isolé car il était albinos, l'autre plus classique,
probablement un opposant politique ou un traître. Toujours est-il qu'aucun
n'ouvre la bouche. L'une des deux portes au nord est ouverte et donne sur une
geôle vide. Le couloir chemine d'est en ouest, vers le hall à l'ouest et
l'entrepôt à l'est.

>e

Couloir de l'entrepôt
Vous avancez dans le corridor qui mène à l'entrepôt principal, là où les rats
entreposent la plupart de leurs conquêtes. La porte de l'entrepôt se trouve à
l'est, tandis qu'à l'ouest vous retournez vers les prisons.

Vous pouvez voir le gardien endormi.

>r gardien
Un vieux rat de constitution imposante, qui vous domine de toute sa grandeur. Il
semble profondément endormi, mais vous préférez rester prudent.

>lancer la boite a l'ouest
Vous lancez la conserve de toutes vos forces vers les prisons et vous glissez
dans une zone d'ombre. Le fracas que sa chute provoque réveille brusquement le
gardien, qui, étonné, s'en va observer la cause du trouble. Dans sa hâte, le
garde somnolant fait tomber les clefs de l'entrepôt au sol.

Il revient une minute plus tard après avoir inspecté le couloir des prisons, et
se rendort aussitôt à son poste.

[Votre score vient d'augmenter de quatre points.]

>r

Couloir de l'entrepôt
Vous avancez dans le corridor qui mène à l'entrepôt principal, là où les rats
entreposent la plupart de leurs conquêtes. La porte de l'entrepôt se trouve à
l'est, tandis qu'à l'ouest vous retournez vers les prisons.

Vous pouvez voir la clef de l'entrepôt et le gardien endormi.

>e
Vous ne pouvez pas, puisque la porte de l'entrepôt est sur votre chemin.

>ouvrir la porte
Elle semble être fermée à clé.

>deverrouiller la porte
[Précisez l'objet concerné par cette action, ou à utiliser.]

>clef
(vous prenez d'abord la clef de l'entrepôt)
Vous déverrouillez la porte à l'aide de la clef de l'entrepôt et l'ouvrez.

>e

Entrepôt du gang de Kay
Circulant au milieu de petites boîtes en carton et de balles luminescentes qui
procurent la faible lumière de ce lieu, vous cherchez désespérément trace du
blason de votre roi, en vain. Dans les boîtes, des vivres en abondance, du pain
rassis principalement, et parfois quelque fromage moisissant dont vous humez les
fragrances avec délectation. Dans un coin, un matelas de paille et un verre
renversé en guise de table, probablement le logis du gardien.

>r fromage
Des morceaux de fromage que vous ne prenez même pas la peine d'identifier, tout
juste bons pour un rat vu leur aspect et leur odeur.

>prendre fromage
Vous saisissez un morceau de fromage commun, que vous ne pouvez même pas
identifier tellement il semble vieux.

>o

Couloir de l'entrepôt
Vous avancez dans le corridor qui mène à l'entrepôt principal, là où les rats
entreposent la plupart de leurs conquêtes. La porte de l'entrepôt se trouve à
l'est, tandis qu'à l'ouest vous retournez vers les prisons.

Vous pouvez voir le gardien endormi.

>o

Couloir des prisons
Au nord, au sud, quatre prisons, dans deux desquelles se morfondent deux rats,
l'un blanc, sûrement isolé car il était albinos, l'autre plus classique,
probablement un opposant politique ou un traître. Toujours est-il qu'aucun
n'ouvre la bouche. L'une des deux portes au nord est ouverte et donne sur une
geôle vide. Le couloir chemine d'est en ouest, vers le hall à l'ouest et
l'entrepôt à l'est.

Vous pouvez voir une boîte de conserve vide.

>o

Hall du repaire de Kay
L'entrée de l'antre de Kay s'enfonce de plusieurs centimètres et passe sous le
mur de la maison. Une dynamo, au moins grosse comme vous, est actionnée par un
vieux rat difforme et aveugle. Selon les pancartes, à l'ouest seraient les
quartiers des membres du gang, à l'est les prisons et l'entrepôt, et au nord la
salle de réunion où se tient probablement le rat Kay lui-même. Au sud, la
lumière, et la sortie.

>n

Couloir vers la salle de réunion
Un long couloir conduit au nord à la salle de réunion, tandis que s'ouvrent à
l'ouest les appartements personnels du rat Kay. Un jeune rat grassouillet garde
la porte au nord, arborant un sourire béat et doté d'un air idiot hors du
commun.

Vous pouvez voir un garde idiot.

>r garde
Un peu grassouillet, il semble avoir découvert récemment qu'il pouvait loucher
et faire des bulles avec sa salive simultanément. Cependant il garde la porte
avec fermeté, et passer ne sera pas aussi facile qu'il est idiot.

>donner fromage au garde
Une idée folle traverse votre esprit. Vous enlevez le papier-toilette qui vous
recouvre ainsi que le foulard qui couvre votre museau et approchez du garde en
arborant un sourire éclatant et en marchant en pleine lumière pour bien vous
faire remarquer.

"Qui c'est qui vient ?" questionne le gros rat. "Tu veux quoi ?"

Toujours souriant, vous prenez un morceau de fromage et le lui présentez :
"Vois-tu, noble ami, j'ai été mandé d'apporter ce met délicieux à notre cher
patron, Kay. Ce pourquoi j'aimerais passer."

"Euh, c'est du poison ?"

"Voyons, mon ami le garde de l'entrepôt ne m'aurait pas donné un fromage
empoisonné pour notre chef. Mais je peux le goûter devant toi si tu veux." Vous
avalez un morceau de fromage au goût affreux. Oubliant la prétendue raison de
votre venue, l'autre s'enquit :

"Et je peux goûter aussi ?"

Lui donnant les morceaux restants, vous passez la porte pendant qu'il dévore
l'amas de moisissure, tout en rééquipant votre accoutrement.

Antichambre de la salle de réunion
Vous vous trouvez dans une petite antichambre juste avant la salle de réunion au
nord où se trouve probablement Kay actuellement. Le hall se trouve au sud.

>s

Couloir vers la salle de réunion
Un long couloir conduit au nord à la salle de réunion, tandis que s'ouvrent à
l'ouest les appartements personnels du rat Kay. Un jeune rat grassouillet garde
la porte au nord, arborant un sourire béat et doté d'un air idiot hors du
commun.

Vous pouvez voir un garde idiot.

>o

Chambre de Kay
Vous ne voyez pas grand chose, excepté une lampe de vélo qui fait ici office de
lumière d'éclairage, de forme circulaire et relativement plate, et qui s'allume
en y exerçant une impulsion.

>allumer lampe
Vous allumez la lampe qui éclaire brillamment le lieu.

>r

Chambre de Kay
La chambre de Kay est lugubre : les murs de terre sèche sont décorés de traces
de pattes à la peinture rouge - ou au sang peut-être, mais mieux vaut ne pas y
songer -, des crânes d'oisillons tombés du nid trônent sur une boîte à musique
reconvertie en bureau, qui joue probablement un hymne si l'on en tourne la
manivelle.
Le lit en est un vrai : un jouet de fillette initialement conçu pour une poupée
sur lequel chaque rat du gang semble avoir écrit un message incongru ou dessiné
quelque obscénité. La sortie est à l'est.

>r boite a musique
[Merci de reformuler. Je n'ai compris que : regarder la boîte à musique.]

>prendre boite
Cela n'a visiblement aucun intérêt.

>tourner boite
Vous en êtes incapable.

>r boite
Une boîte à musique noire, sertie de petites vis dorées, avec une manivelle sur
son côté droit, vers le lit. Trois crânes d'oisillons sont posés sur le
couvercle de la boîte, presque contre le mur.

>tourner manivelle
Vous tournez la manivelle non sans difficulté, et le capot se soulève lentement,
faisant rouler les crânes encore plus contre le mur dans un bruit étouffé. Une
musique effrayante, jouant des basses et des gammes mineures comme un jongleur
avec ses balles, en jaillit, tandis que vous découvrez à l'intérieur de la boîte
un carnet usé, qui se révèle être le journal de Kay. Vous vous en saisissez et
refermez le couvercle de la boîte.

>i
Vous avez :
  le journal de Kay
  la clef de l'entrepôt
  un poignard d'acier trempé
  un morceau de parchemin
  des feuilles de papier toilette (portées)
  un foulard noir (porté)
  un parchemin royal

>r journal
Vous feuilletez attentivement le carnet qui liste tous les méfaits et états
d'âme de Kay, et se termine ainsi :

Jour 22 de l'année du Reblochon,

J'avais intuité que la souris Stanley me serait utile tôt ou tard, voilà qui est
chose faite : l'ancien Chevalier de Mice VII, avide de vengeance, m'a apporté le
blason de la famille royale sans que je n'aie besoin de rien lui demander. Il
s'est par cet acte offert une place parmi nous, bien que je continue de le
surveiller.
Vue la valeur de l'artefact, je vais le placer au-dessus de la porte de la salle
de réunion, ainsi je l'aurai toujours en vue, dès que j'aurais fait planter un
clou.

Voilà donc l'information qu'à la fois vous attendiez et redoutiez. Le voleur est
bien Stan, qui faisait partie du trio que vous formiez avec le roi et qui vous
semblait, alors jeunes souriceaux, inébranlable. Mais voilà plusieurs lunes que
Stanley avait quitté le palais suite à une altercation avec son ancien ami le
roi Mice VII, et vous étiez jusqu'à aujourd'hui sans nouvelles.

[Votre score vient d'augmenter de un point.]

>r morceau
C'est le bas d'une petite de parchemin à échelle de souris, un genre de post-it
en fait, qui a été en partie dévoré par le feu. Vous ne pouvez y lire plus que
trois mots :

"pour le blason."

Et le tout est signé : Stanley.



>r poignard
Une lame en acier trempé de petite taille montée sur une garde à votre échelle
constitue ce poignard. Vous vous demandez si les humains ont eu la hardiesse de
concevoir d'aussi petites armes blanches ou si la technologie des rats dépasse
tout ce que vous pouviez imaginer. Sur la lame est gravé le nom Stanley.

>objets
Objets ayant été manipulés :

la paille de plastique   (Sous le vieux buffet)
le parchemin royal   (dans l'inventaire)
le foulard noir   (sur le corps)
les feuilles de papier toilette   (sur le corps)
le morceau de parchemin   (dans l'inventaire)
le poignard d'acier trempé   (dans l'inventaire)
la clef de l'entrepôt   (dans l'inventaire)
la boîte de conserve vide   (Couloir des prisons)
les morceaux de fromage   (n'existe plus)
le journal de Kay   (dans l'inventaire)

>e

Couloir vers la salle de réunion
Un long couloir conduit au nord à la salle de réunion, tandis que s'ouvrent à
l'ouest les appartements personnels du rat Kay. Un jeune rat grassouillet garde
la porte au nord, arborant un sourire béat et doté d'un air idiot hors du
commun.

Vous pouvez voir un garde idiot.

>n
"Tu veux d'jà r'passer ? D'accord..."

Antichambre de la salle de réunion
Vous vous trouvez dans une petite antichambre juste avant la salle de réunion au
nord où se trouve probablement Kay actuellement. Le hall se trouve au sud.

>n
Vous jetez un œil en entrouvrant la porte au nord, et vous voyez Kay et deux
acolytes en train de discuter autour d'une table en plastique jaune. De nombreux
éclats de miroir constellent les parois de la salle et lui donnent une allure
étrange. Le blason est au milieu d'eux, sur la table.

Vous revenez dans l'antichambre afin de vous assurer une dernière fois que vous
avez une idée de comment vous emparer du blason.

>n
Vous pénétrez tout pataud dans la salle de réunion tandis que les trois rats
vous fixent, pétrifiés par la stupeur. Tant et si bien que vous parvenez à
avancer jusqu'à eux, à saisir le tableau, et à faire demi-tour jusqu'à
l'antichambre quand Kay sort de sa torpeur :

"Quel est ce type ridicule recouvert de papier-toilette ?"

Et ne trouvant aucun écho à sa question, Kay se lance à votre poursuite.

[Votre score vient d'augmenter de cinq points.]

>s

Couloir vers la salle de réunion
Un long couloir conduit au nord à la salle de réunion, tandis que s'ouvrent à
l'ouest les appartements personnels du rat Kay. Un jeune rat grassouillet garde
la porte au nord, arborant un sourire béat et doté d'un air idiot hors du
commun.

Vous pouvez voir un garde idiot.

>s

Hall du repaire de Kay
L'entrée de l'antre de Kay s'enfonce de plusieurs centimètres et passe sous le
mur de la maison. Une dynamo, au moins grosse comme vous, est actionnée par un
vieux rat difforme et aveugle. Selon les pancartes, à l'ouest seraient les
quartiers des membres du gang, à l'est les prisons et l'entrepôt, et au nord la
salle de réunion où se tient probablement le rat Kay lui-même. Au sud, la
lumière, et la sortie.

>s
Vous faites une nouvelle apparition fantasque dans votre accoutrement et manquez
de tuer le garde en l'effrayant à nouveau.

Au plus profond de la cave
La cave de la maison est extrêmement petite, et ne sert qu'à entreposer le vin,
les outils et vêtements de jardin du père. L'obscurité est quasi totale puisque
seul un soupirail vient éclairer le lieu, mais vous voyez néanmoins l'entrée du
repaire de Kay sur le mur nord. L'escalier remonte à la cuisine à l'est.

>e

La cuisine
Relativement exiguë, la cuisine est plutôt calme, les enfants étant plongés dans
leurs activités numériques tandis que la mère se trouve dans sa chambre. Du
robinet au-dessus de l'évier goutte à intervalle régulier une petite note d'eau
qui vient résonner de son ploc dans l'atmosphère moite de la pièce. Au sud se
trouve le couloir menant au salon et aux chambres, tandis qu'à l'ouest un
escalier descend vers la cave et qu'à l'est se tient la porte d'entrée de la
maison.

>s

Couloir vers la cuisine
L'unique couloir de la maison qui joint, du nord au sud, la cuisine et les
chambres. À l'est se trouvent la salle de bain dont la porte est fermée, et les
toilettes, dont l'accès vous est désormais possible.

>o

Sous le bureau de l'ordinateur
Vous vous glissez sous le bureau de l'ordinateur familial, un énorme engin au
ronronnement inquiétant qui résonne de mille clics sous l'action effrénée du
fils de la maison, scotché devant un écran d'une taille démesurée, casque sur
les oreilles, et gesticulant visiblement aux prises avec des terroristes
virtuels. À l'ouest vous retrouvez le calme de votre buffet, le téléviseur et
plus loin le balcon, au nord-ouest s'ouvre toujours un espace vide et
étrangement silencieux et à l'est se trouve l'entrée d'un couloir.

Vous pouvez voir le fils.

>o

Sous le vieux buffet
Vous voilà devant l'entrée du palais royal, contre le mur sud du salon, sous le
vieux buffet qui siège entre l'ordinateur à l'est et le téléviseur à l'ouest. Au
nord vous êtes au beau milieu de la pièce, à découvert, et à la merci de
Choupinette.

Vous pouvez voir une paille de plastique.

>s

Palais du roi Mice
Vous êtes devant le trône de votre roi, Mice VII, dressé dans une cavité creusée
dans le béton par les pattes de vos ancêtres, derrière le vieux buffet du salon.
Le roi grignote un morceau de fromage en plastique sur son trône, d'où il
contemple ses sujets affairés dans la pièce.
La porte est au nord, tandis qu'à l'est s'étendent les dortoirs.

Vous pouvez voir des sujets de la cour et le roi Mice VII.

À votre entrée dans la salle du trône, tous les regards se tournent sur vous et
le blason que vous portez au bras. Les chuchotements vont bon train et peu à peu
une clameur s'élève, jusqu'à ce que le roi se rende compte de la situation.

"Ahem...", commence-t-il, et tous les badauds se taisent et l'observent. "Mon
cher ami, je crois voir que ta mission a été un succès..."

Vous hochez de la tête et avancez vers le trône.

"... et je t'en félicite, tout comme je m'en félicite de t'avoir désigné. As-tu
cependant trouvé quel était le voleur ?"

Vous lui tendez le journal de Kay, le morceau de parchemin et le poignard trouvé
dans le dortoir.

"Sire...", hésitez-vous, "Il n'est autre que Stanley..."

"Stan ?" rugit le roi. "Le traître... Nous en parlerons en privé mon ami. Pour
l'heure, festoyons, mes neveux viennent de me rapporter du gruyère sans trous
qu'ils ont trouvé ce matin."

Vous tendez le blason au roi qui l'accroche à nouveau au-dessus de son trône, et
les réjouissances commencent.

    *** Vous avez gagné ***


Dans cette partie vous avez obtenu 36 sur un score possible de 40, en 83 tours.

Voulez-vous RECOMMENCER, CHARGER une partie sauvegardée, ANNULER votre dernière
action, obtenir le score détaillé pour cette partie (FULLSCORE) ou QUITTER ?
> quitter

