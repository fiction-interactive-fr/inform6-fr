## Tests de non régression de la bibliothèque francophone pour Inform 6

Se référer au fichier .gitlab-ci.yml pour connaître la manière exacte dont ces tests sont
exécutés dans le pipeline de CI.

Mais le but n'est pas d'attendre le verdict du pipeline: ils sont aussi conçus pour pouvoir
être exécutés localement (peut-être quelques adaptations seront-elles nécessaires pour l'exécution
sur Windows ou Mac OS).

En quelques mots :
- le binaire inform6 doit être disponible sur le système ;
- il faut exécuter le script tools/prepare.sh afin d'installer et/ou compiler les divers
  outils nécessaires : la bibliothèque inform6lib dans la version supportée par
  la bibliothèque francophone (au 23 juillet 2024, il s'agit de la version 6.11),
  frotz (plus précisément, le binaire dfrotz), glulxe compilé avec le backend cheapglk,
  le repository plotex qui contient le script regtest.py.
- lancer `make build` pour compiler les versions .z8 et .ulx des jeux servant de support aux tests.
- lancer `make test` pour exécuter les tests.
- ... Ou simplement `make` pour faire les deux.

Il y a deux types de tests :
- comparaison de transcripts. Regardez le contenu du dossier `kit_base` pour les éléments qui
  entrent dans la composition de ces tests : listes de commandes, transcripts de référence...
- RegTest, définis dans des fichiers dont le suffixe est .test. Se référer à la documentation
  de RegTest: https://eblong.com/zarf/plotex/regtest.html pour leur syntaxe. En plus d'éxécuter
  ces tests et de faire les vérifications incluses dans leur définition, on produit également
  un transcript à partir de leur exécution, que l'on compare à un transcript de référence.

La comparaison des transcripts produits aux transcripts de référence se fait en utilisant git,
afin de simplifier le traitement des différences détectées, et leur éventuel enregistrement
en tant que nouvelle référence.

L'idée est la suivante : la référence, c'est la version du transcript
qui est dans HEAD, ou en staging. Toute différence entre l'état du
fichier et la référence connue de git fait échouer le test. Si la
différence est normale car due à un changement attendu, alors il n'y a
plus qu'à utiliser `git add -p` pour valider les changements et en faire
un commit.

Exemples de contenus de répertoires :
```
kit_base/
├── kit_base.inf            fichier source
├── kit_base.ulx            fichier compilé glulxe. Non inclus dans le repo git. Produit par `make build`
├── kit_base.z8             fichier compilé z-code. Non inclus dans le repo git. Produit par `make build`
├── recording.txt           commandes à exécuter en jeu pour produire un transcript à comparer à la référence
├── run-cmds.txt            commandes de contrôle du test qui va produire le transcript ("script", "replay"...)
├── script_ulx.txt          transcript de référence pour la version ulx
└── script_z8.txt           transcript de référence poru la version z8

epreuve/
├── epreuve.inf             fichier source
├── epreuve.ulx             fichier compilé glulxe. Non inclus dans le repo git. Produit par `make build`
├── epreuve.z8              fichier compilé z-code. Non inclus dans le repo git. Produit par `make build`
├── README.md               des infos sur le jeu de test
├── simple.test             un test RegTest
├── simple.test.ulx_output  transcript de référence pour la version ulx du jeu pour le test simple.test
└── simple.test.z8_output   transcript de référence pour la version z8 du jeu pour le test simple.test
```

Pour le détail de l'exécution des tests, se référer au contenu du fichier `makefile`.
