#!/bin/bash

# Preparation pour pouvoir faire tourner les tests.
# Il s'agit de récupérer dans le répertoire tools/ les outils et libs nécessaires.
# Ainsi que de compiler certains outils.
# Pour ce qui est du compilateur inform6 lui-même, on part du principe qu'il sera
# installé indépendemment.

set -eux

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
pushd "$SCRIPT_DIR"

rm inform6lib -rf && git clone --branch=6.11 https://gitlab.com/DavidGriffith/inform6lib inform6lib

rm plotex -rf && git clone https://github.com/erkyrath/plotex

rm cheapglk -rf && git clone https://github.com/erkyrath/cheapglk
cd cheapglk
make
cd ..

rm glulxe -rf && git clone https://github.com/erkyrath/glulxe
cd glulxe
# On suppose un Linux, mais à améliorer si ça pose problème sur MAC ou WINDOWS
sed -i 's/\(OPTIONS = .*-DOS_\)MAC/\1UNIX/' Makefile
make
cd ..

rm frotz -rf && git clone https://gitlab.com/DavidGriffith/frotz
cd frotz
make dfrotz
cd ..
