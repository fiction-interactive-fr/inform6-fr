INFORM6 ?= inform
INFORM6LIB ?= tools/inform6lib/
TERP ?= ../tools/frotz/dfrotz
TERP_OPTS ?= -S 80 -Z3 -q
TEST_ULX = 1
ULX_TERP ?= ../tools/glulxe/glulxe
ULX_TERP_OPTS ?= -q -u
REGTEST ?= python ../tools/plotex/regtest.py
IGNORE_REGTEST_DIFF ?= 0

JEUX = kit_base rats epreuve

all: build test

build:
	@failed=0; \
	failed_tests=""; \
	for jeu in $(JEUX); do \
	  cd $${jeu}; \
	  echo -e "\n*** Building: $${jeu} (z8) ***" && \
	  $(INFORM6) -Cu -D -v8 +include_path=./,../../,../$(INFORM6LIB) +Language_name=French $${jeu}.inf; \
	  if [[ $$? != 0 ]]; then ((failed+=1)); failed_tests+="$${jeu} (z8 build) "; fi; \
	  if [ $(TEST_ULX) -eq 1 ]; then \
	    echo -e "\n*** Building: $${jeu} (ulx) ***" && \
	    $(INFORM6) -Cu -D -G +include_path=./,../../,../$(INFORM6LIB) +Language_name=French $${jeu}.inf; \
	    if [[ $$? != 0 ]]; then ((failed+=1)); failed_tests+="$${jeu} (ulx build) "; fi; \
	  fi; \
	  cd ..; \
	done; \
	if [[ $$failed != 0 ]]; then \
	  echo -e "\n### The following $$failed build(s) failed: $$failed_tests\n### See above log output for details.\n"; \
	  exit $$failed; \
	fi

test:
	@failed=0; \
	failed_tests=""; \
	for jeu in $(JEUX); do \
	  cd $${jeu}; \
	  if [ -f run-cmds.txt ]; then \
	    echo -e "\n*** Testing: $${jeu} (z8 transcript) ***" && \
	    rm -f script.txt && \
	    $(TERP) $(TERP_OPTS) $${jeu}.z8 < run-cmds.txt > /dev/null && \
	    mv -f script.txt script_z8.txt && \
	    git diff --exit-code -- script_z8.txt; \
	    if [[ $$? != 0 ]]; then ((failed+=1)); failed_tests+="$${jeu} (z8 transcript) "; fi; \
	    if [ $(TEST_ULX) -eq 1 ]; then \
	      echo -e "\n*** Testing: $${jeu} (ulx transcript) ***" && \
	      rm -f script.txt && \
	      $(ULX_TERP) $(ULX_TERP_OPTS) $${jeu}.ulx < run-cmds.txt > /dev/null && \
	      iconv -f ISO8859-15 -t UTF-8 script.txt > script_ulx.txt && \
	      rm -f script.txt && \
	      git diff --exit-code -- script_ulx.txt; \
	      if [[ $$? != 0 ]]; then ((failed+=1)); failed_tests+="$${jeu} (ulx transcript) "; fi; \
	    fi; \
	  fi; \
	  for regtest in *.test; do \
	    [[ "$${regtest}" = "*.test" ]] && continue; \
	    echo -e "\n*** Testing: $${jeu} (z8 $${regtest}) ***" && \
	    $(REGTEST) "$${regtest}" -g $${jeu}.z8 -i "$(TERP) $(TERP_OPTS)"; \
	    if [[ $$? != 0 ]]; then ((failed+=1)); failed_tests+="$${jeu} (z8 $${regtest}) "; fi; \
	    $(REGTEST) "$${regtest}" -g $${jeu}.z8 -i "$(TERP) $(TERP_OPTS)" -v > $${regtest}.z8_output; \
	    sed -i '/^Release\s.*Serial\s.*Inform\s.*$$/d' $${regtest}.z8_output; \
	    [ $(IGNORE_REGTEST_DIFF) -eq 1 ] || git diff --exit-code -- $${regtest}.z8_output; \
	    if [[ $$? != 0 ]]; then ((failed+=1)); failed_tests+="$${jeu} (z8 $${regtest} diff) "; fi; \
	    if [ $(TEST_ULX) -eq 1 ]; then \
	      echo -e "\n*** Testing: $${jeu} (ulx $${regtest}) ***" && \
	      $(REGTEST) "$${regtest}" -g $${jeu}.ulx -i "$(ULX_TERP) $(ULX_TERP_OPTS)"; \
	      if [[ $$? != 0 ]]; then ((failed+=1)); failed_tests+="$${jeu} (ulx $${regtest}) "; fi; \
	      $(REGTEST) "$${regtest}" -g $${jeu}.ulx -i "$(ULX_TERP) $(ULX_TERP_OPTS)" -v > $${regtest}.ulx_output; \
	      sed -i '/^Release\s.*Serial\s.*Inform\s.*$$/d' $${regtest}.ulx_output; \
	      [ $(IGNORE_REGTEST_DIFF) -eq 1 ] || git diff --exit-code -- $${regtest}.ulx_output; \
	      if [[ $$? != 0 ]]; then ((failed+=1)); failed_tests+="$${jeu} (ulx $${regtest} diff) "; fi; \
	    fi; \
	  done; \
	  cd ..; \
	done; \
	if [[ $$failed != 0 ]]; then \
	  echo -e "\n### The following $$failed test(s) failed: $$failed_tests\n### See above log output for details.\n"; \
	  exit $$failed; \
	fi

clean:
	@for jeu in $(JEUX); do \
	  cd $${jeu} && \
	  rm -f $${jeu}.z8 $${jeu}.ulx && \
	  cd ..; \
	done
