La bibliothèque francophone pour Inform 6 est maintenue par la communauté
francophone de fiction interactive [Fiction-interactive.fr](http://www.fiction-interactive.fr/),
anciennement appelée [IFiction-FR](http://ifiction.free.fr/).

Ce projet fut initié par Jean-Luc Pontico de 2001 à 2004, puis maintenu par
de nombreux contributeurs, listés ci-dessous en ordre alphabétique.

- Adrien Saurat
- Éric Forgeot
- Eriorg
- FibreTigre
- Hugo Labrande
- Jean-Luc Pontico
- Nathanaël Marion
- Rémi Verschelde
- Samuel Verschelde
