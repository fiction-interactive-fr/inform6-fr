# Bibliothèques francophones pour Inform 6

Ces bibliothèques sont destinées à être utilisées avec Inform 6 pour la
réalisation de jeux d'aventures textuels en français.

- [Site officiel](http://www.fiction-interactive.fr/creer/inform-6/) et
  téléchargement de la dernière version stable.
- [Code source](https://gitlab.com/fiction-interactive-fr/inform6-fr), pour
  tester la version de développement ou signaler des problèmes.
- [Fiction-interactive.fr](http://www.fiction-interactive.fr/creer/inform-6/):
  carrefour francophone de la fiction interactive (jeux, articles, forum, etc.).


## Contenu des bibliothèques

- `French.h`: 2ème personne du pluriel, présent.
- `FrenchG.h`: 2ème personne du pluriel, présent (grammaire).


## Utilisation

Si vous ignorez comment compiler un jeu avec Inform 6, consultez le tutoriel
[Écrire une fiction interactive avec Inform 6](
http://www.fiction-interactive.fr/ecrire-une-fiction-interactive-avec-inform-6-partie-1-demarrer/).

Le site [Fiction-interactive.fr](http://www.fiction-interactive.fr) contient
de nombreuses informations, un kit de démarrage rapide, et des liens pour
communiquer avec d'autres utilisateurs d'Inform 6 sur le forum ou Discord.


## Encodage

Les fichiers sont encodés en UTF-8. Vos sources doivent également être encodées en UTF-8,
et le tout compilé avec l'option `-Cu` d'inform.

UTF-8 est le seul encodage officiellement supporté, mais vous pouvez convertir à votre guise
les fichiers vers latin1 (ISO8859-1), si vraiment nécessaire pour vous. De nombreux outils
permettent ce genre de conversion.


## Compilateur

Le compilateur inform6 en version supérieure ou égale à 6.33 est nécessaire, en raison
de l'encodage en UTF-8.


## Compatibilité avec inform6lib

La bibliothèque inform officielle, anglophone, est nécessaire en plus des fichiers de traduction
que nous fournissons.

La version actuellement supportée est la version 6.11.


## Licence

Tous les fichiers de ce dépôt sont distribués sous la licence Aristic 2.0,
telle que décrite dans le fichier [ARTISTIC](/ARTISTIC) ainsi que les
en-têtes de chaque fichier des bibliothèques francophones.

Se référer au fichier [COPYING](/COPYING) pour de plus amples détails, et au
fichier [AUTEURS.md] pour la liste des contributeurs.
