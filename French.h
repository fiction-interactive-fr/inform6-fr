! ==============================================================================
!   French.h:  Bibliothèque pour le support de la langue française
!
!   Conçue pour Inform 6 -- Release 6/11 -- Serial number 040227
!
! ------------------------------------------------------------------------------
!
!   Cette bibliothèque est dérivée du fichier `english.h` de la bibliothèque
!   standard pour Inform 6 (https://gitlab.com/DavidGriffith/inform6lib) par
!   Graham Nelson et David Griffith.
!   Comme la bibliothèque standard, cette adaptation pour le français est
!   distribuée sous la licence Artistic License 2.0 (cf. COPYING et ARTISTIC).
!
!   Copyright (c) 2004-2021 Communauté francophone de fiction interactive
!                           (cf. AUTEURS.md)
!   Copyright (c) 2001-2004 Jean-Luc Pontico
!   Copyright (c) 1993-2004 Graham Nelson
!
! ------------------------------------------------------------------------------
!
!   Pour utiliser cette bibliothèque, passez l'option "+language_name=French"
!   au compilateur Inform via votre commande de compilation, ou l'en-tête de
!   votre code source.
!
! ==============================================================================

! ------------------------------------------------------------------------------
!   Hacks dans les routines de la bibliothèque (Ajout FR)
! ------------------------------------------------------------------------------

! Important : Ces hacks doivent être faits avant la déclaration de `System_file;`.

! Quand il y a les prépositions 'a'/'au'/'aux' et 'de'/'du'/'des'/'d^', et qu'il faut
! compléter le motif, le parser prend toujours la première préposition pour l'affichage
! ce qui donne "se servir (de le passeport)"

Replace PrintCommand;

[ PrintCommand from i k spacing_flag variante_flag prep;
    if (from == 0) {
        i = verb_word;
        if (LanguageVerb(i) == 0)
            if (PrintVerb(i) == 0) print (address) i;
        from++; spacing_flag = true;
    }

    variante_flag = 0;
    for (k = from : k < pcount : k++) {
        i = pattern-->k;
        if (i == PATTERN_NULL) continue;
        if (spacing_flag && variante_flag == 0) print (char) ' ';
        if (i == 0) { print (string) THOSET__TX; jump TokenPrinted; }
        if (i == 1) { print (string) THAT__TX;   jump TokenPrinted; }
        if (i >= REPARSE_CODE) {
            prep = No__Dword(i - REPARSE_CODE);
            if (prep == 'de' || prep == 'a//') {
                if (prep == 'de') { variante_flag = 1; } else { variante_flag = 2; }
                ! On skippe, on affichera tout ça au tour suivant
            }
            else { print (address) prep; }
        }
        else {
            if (i in compass && LanguageVerbLikesAdverb(verb_word))
                LanguageDirection (i.door_dir); ! the direction name as adverb
            else if (variante_flag > 0) {
                if (variante_flag == 1) { print (DeDuDes) i; }
                else { print (to_the) i; }
                variante_flag = 0;
            }
            else { print (the) i; }
        }
        .TokenPrinted;
        spacing_flag = true;
    }
];

! ------------------------------------------------------------------------------
!   Début de la lib en tant que telle
! ------------------------------------------------------------------------------

Constant LibReleaseFR "6.11.4dev";
! Message ne permet pas d'inclure une constante, cela doit être d'un seul bloc.
Message "[Compilé avec la version 6.11.4dev de la bibliothèque francophone.]";

System_file;

! ------------------------------------------------------------------------------
!   Constantes pour les variantes régionales
! ------------------------------------------------------------------------------

! Signification des valeurs :
! - VARIANTE_70 : 0 pour soixante-dix, 1 pour septante.
! - VARIANTE_80 : 0 pour quatre-vingts, 1 pour huitante, 2 pour octante.
! - VARIANTE_90 : 0 pour quatre-vingt-dix, 1 pour nonante.

! Des valeurs par défaut pour la Belgique.
#Ifdef DIALECT_BELGIUM;
Default VARIANTE_70 1; ! Septante.
Default VARIANTE_90 1; ! Nonante.
#Endif;

! Des valeurs par défaut pour la Suisse.
#Ifdef DIALECT_SWITZERLAND;
Default VARIANTE_70 1; ! Septante.
Default VARIANTE_80 1; ! Huitante.
Default VARIANTE_90 1; ! Nonante.
#Endif;

! Les valeurs par défaut quand on n'utilise pas de DIALECT_*.
Default VARIANTE_70 0; ! Soixante-dix.
Default VARIANTE_80 0; ! Quatre-vingts.
Default VARIANTE_90 0; ! Quatre-vingt-dix.

! ------------------------------------------------------------------------------
!   Part I.   Preliminaries
! ------------------------------------------------------------------------------

!Constant EnglishNaturalLanguage;    ! Needed to keep old pronouns mechanism

Class   CompassDirection
  with  number 0, article "le",
        description [;
            if (location provides compass_look && location.compass_look(self)) rtrue;
            if (self.compass_look()) rtrue;
            L__M(##Look, 7, self);
        ],
        compass_look false,
  has   scenery;

Object Compass "compass" has concealed;

#Ifndef WITHOUT_DIRECTIONS;
CompassDirection -> n_obj "nord"
                    with door_dir n_to, name 'n//' 'nord';
CompassDirection -> s_obj "sud"
                    with door_dir s_to, name 's//' 'sud';
CompassDirection -> e_obj "est"
                    with door_dir e_to, name 'e//' 'est',
                    article "l'";
CompassDirection -> w_obj "ouest"
                    with door_dir w_to, name 'o//' 'w//' 'ouest',
                    article "l'";
CompassDirection -> ne_obj "nord-est"
                    with door_dir ne_to, name 'ne' 'nordest';
CompassDirection -> nw_obj "nord-ouest"
                    with door_dir nw_to, name 'no' 'nw' 'nordouest';
CompassDirection -> se_obj "sud-est"
                    with door_dir se_to, name 'se' 'sudest';
CompassDirection -> sw_obj "sud-ouest"
                    with door_dir sw_to, name 'so' 'sw' 'sudouest';
CompassDirection -> u_obj "haut"
                    with door_dir u_to, name 'h//' 'haut' 'plafond' 'ciel';
CompassDirection -> d_obj "bas"
                    with door_dir d_to, name 'b//' 'd//' 'bas' 'sol';
#endif; ! WITHOUT_DIRECTIONS

CompassDirection -> in_obj "intérieur"
                    with door_dir in_to, name 'dedans' 'interieur',
                    article "l'";
CompassDirection -> out_obj "extérieur"
                    with door_dir out_to, name 'dehors' 'exterieur',
                    article "l'";

! ------------------------------------------------------------------------------
!   Part II.   Vocabulary
! ------------------------------------------------------------------------------

Constant AGAIN1__WD   = 'encore';
Constant AGAIN2__WD   = 'g//';
Constant AGAIN3__WD   = 'c//';
Constant OOPS1__WD    = 'oops';
Constant OOPS2__WD    = 'oups';
Constant OOPS3__WD    = 'euh';
Constant UNDO1__WD    = 'undo';
Constant UNDO2__WD    = 'annule';
Constant UNDO3__WD    = 'annuler';

Constant ALL1__WD     = 'tous';
Constant ALL2__WD     = 'toutes';
Constant ALL3__WD     = 'tout';
Constant ALL4__WD     = 'tout';
Constant ALL5__WD     = 'tout';
Constant AND1__WD     = 'et';
Constant AND2__WD     = 'et';
Constant AND3__WD     = 'et';
Constant BUT1__WD     = 'mais pas'; ! FIXME : ne fonctionne pas
Constant BUT2__WD     = 'excepte';
Constant BUT3__WD     = 'sauf';
Constant ME1__WD      = 'moi';
Constant ME2__WD      = 'toi'; ! FIXME : nous ? moi-même ? - EN myself
Constant ME3__WD      = 'vous'; ! 'vous' comme alias interne désignant le joueur
Constant OF1__WD      = 'de';
Constant OF2__WD      = 'de';
Constant OF3__WD      = 'de';
Constant OF4__WD      = 'de';
Constant OTHER1__WD   = 'autre';
Constant OTHER2__WD   = 'autre';
Constant OTHER3__WD   = 'autre';
Constant THEN1__WD    = 'alors';
Constant THEN2__WD    = 'puis';
Constant THEN3__WD    = 'ensuite';

Constant NO1__WD      = 'n//';
Constant NO2__WD      = 'non';
Constant NO3__WD      = 'non';
Constant YES1__WD     = 'o//';
Constant YES2__WD     = 'y//';
Constant YES3__WD     = 'oui';

Constant AMUSING__WD  = 'amusing';
Constant FULLSCORE1__WD = 'fullscore';
Constant FULLSCORE2__WD = 'full';
Constant QUIT1__WD    = 'q//';
Constant QUIT2__WD    = 'quitter';
Constant RESTART__WD  = 'recommencer';
Constant RESTORE__WD  = 'charger';

Array LanguagePronouns table

  ! word        possible GNAs                   connected
  !             to follow:                      to:
  !             a     i
  !             s  p  s  p
  !             mfnmfnmfnmfn

    ! ----- Pronoms compléments directs
    ! Tirets importants pour éviter la confusion avec l'article le/la/les,
    ! par exemple "mange la pomme" est compris comme "mange-la" si "pomme" est inconnu,
    ! d'où des messages d'erreur troublants pour le joueur.
    '-le'     $$100000100000                    NULL
    '-la'     $$010000010000                    NULL
    '-les'    $$000110000110                    NULL
    '-lui'    $$110000110000                    NULL
    '-leur'   $$000110000110                    NULL

    ! ----- Pronoms disjonctifs
    ! Féminin accepté pour 'luy' (mot bidon) pour traiter les cas 'dedans',
    ! 'dessus', 'l^'... (genre inconnu).
     ! "l'ouvrir" devient "ouvrir luy", "luy" étant m ou f.
    ! En fait, '-lui' pourrait jouer le même rôle.
    'luy'     $$110000110000                    NULL
    'lui'     $$100000100000                    NULL
    'elle'    $$010000010000                    NULL
    'eux'     $$000110000110                    NULL
    'elles'   $$000010000010                    NULL;

Array LanguageDescriptors table

  ! word        possible GNAs   descriptor      connected
  !             to follow:      type:           to:
  !             a     i
  !             s  p  s  p
  !             mfnmfnmfnmfn

    ! FIXME: Commentaire à clarifier, ou bug à corriger.
    ! ce qui suit ne doit pas fonctionner souvent, du moins je l'espère
    ! car mon/ma/mes devrait changer en fonction du type de parole
    ! (quoique ça a l'air tres tolerant).
    'mon'     $$100000100000    POSSESS_PK      0
    'ma'      $$010000010000    POSSESS_PK      0
    'mes'     $$000110000110    POSSESS_PK      0
    'ton'     $$100000100000    POSSESS_PK      0
    'ta'      $$010000010000    POSSESS_PK      0
    'tes'     $$000110000110    POSSESS_PK      0
    'notre'   $$110000110000    POSSESS_PK      0
    'nos'     $$000110000110    POSSESS_PK      0
    'votre'   $$110000110000    POSSESS_PK      0
    'vos'     $$000110000110    POSSESS_PK      0
    ! Tirets importants, cf. note pour pronoms compléments directs.
    'son'     $$100000100000    POSSESS_PK      '-lui'
    'sa'      $$010000010000    POSSESS_PK      '-lui'
    'ses'     $$000110000110    POSSESS_PK      '-lui'
    'leur'    $$110000110000    POSSESS_PK      '-les'
    'leurs'   $$000110000110    POSSESS_PK      '-les'

    'le'      $$100000100000    DEFART_PK       NULL
    'la'      $$010000010000    DEFART_PK       NULL
    'l^'      $$110000110000    DEFART_PK       NULL
    'les'     $$000110000110    DEFART_PK       NULL
    'un'      $$100000100000    INDEFART_PK     NULL
    'une'     $$010000010000    INDEFART_PK     NULL
    'des'     $$000110000110    INDEFART_PK     NULL

    'allumé'  $$100000100000    light           NULL
    'allumée' $$010000010000    light           NULL
    'éteint'  $$100000100000    (-light)        NULL
    'éteinte' $$010000010000    (-light)        NULL;

! Ce tableau est utilisé par Inform pour comprendre les nombres en lettres dans
! les commandes du joueur, mais on l'utilise aussi pour afficher les nombres en
! lettres (dans la routine LanguageNumber). C'est pourquoi on a mis le 'une' à
! la fin : ça permet d'avoir une formule pour obtenir l'indice du mot
! correspondant à un nombre.
Array LanguageNumbers table
    'un' 1 'deux' 2 'trois' 3 'quatre' 4 'cinq' 5
    'six' 6 'sept' 7 'huit' 8 'neuf' 9 'dix' 10
    'onze' 11 'douze' 12 'treize' 13 'quatorze' 14 'quinze' 15
    'seize' 16 'dix-sept' 17 'dix-huit' 18 'dix-neuf' 19 'vingt' 20
    'vingt et un' 21 'vingt-deux' 22 'vingt-trois' 23 'vingt-quatre' 24 'vingt-cinq' 25
    'vingt-six' 26 'vingt-sept' 27 'vingt-huit' 28 'vingt-neuf' 29 'trente' 30
    'une' 1;

! ------------------------------------------------------------------------------
!   Part III.   Translation
! ------------------------------------------------------------------------------

! Routines utiles pour reconnaître les versions accentuées d'une lettre

[ IsAnA c;
    ! aàáâãäæå + majuscules
    #Ifdef TARGET_ZCODE;
    if (c == 'a' or 'A' or 155 or 158 or 169 or 175 or 181 or 186 or 191 or 196 or 201 or 202 or 205 or 208 or '@ae' or 212) return true;
    #Ifnot; ! TARGET_GLULX    - donc il faut les valeurs Unicode !
    if ((c == 'a' or 'A') || (c > 191 && c < 199) || (c > 223 && c < 231)) return true;
    #Endif; ! TARGET_
    return false;
];

[ IsAnE c;
    ! eéèêë + majuscules
    #Ifdef TARGET_ZCODE;
    if (c == 'e' or 'E' or 164 or 167 or 170 or 176 or 182 or 187 or 192 or 197 ) return true;
    #Ifnot; ! TARGET_GLULX    - donc il faut les valeurs Unicode !
    if ((c == 'e' or 'E') || (c > 199 && c < 204) || (c > 231 && c < 236)) return true;
    #Endif; ! TARGET_
    return false;
];

[ IsAnI c;
    ! iïíìî + majuscules
    #Ifdef TARGET_ZCODE;
    if (c == 'i' or 'I' or 165 or 168 or 171 or 177 or 183 or 188 or 193 or 198 ) return true;
    #Ifnot; ! TARGET_GLULX    - donc il faut les valeurs Unicode !
    if ((c == 'i' or 'I') || (c > 203 && c < 208) || (c > 235 && c < 240)) return true;
    #Endif; ! TARGET_
    return false;
];

[ IsAnO c;
    ! oóòôõö oe + majuscules
    #Ifdef TARGET_ZCODE;
    if (c == 'o' or 'O' or 156 or 159 or 172 or 178 or 184 or 189 or 194 or 199 or 203 or 204 or 207 or 210 or '@oe' or 221 ) return true;
    #Ifnot; ! TARGET_GLULX    - donc il faut les valeurs Unicode !
            ! les valeurs 338 et 339 (oe et OE) seront transformées automatiquement en ? par Glulx avant même cette fonction, donc ça ne marchera pas
    if ((c == 'o' or 'O') || (c > 209 && c < 215) || (c == 216 or 248 or 338 or 339) || (c > 241 && c < 247)) return true;
    #Endif; ! TARGET_
    return false;
];

[ IsAnU c;
    ! uüùúû + majuscules
    #Ifdef TARGET_ZCODE;
    if (c == 'u' or 'U' or 157 or 160 or 173 or 179 or 185 or 190 or 195 or 200 ) return true;
    #Ifnot; ! TARGET_GLULX    - donc il faut les valeurs Unicode !
    if ((c == 'u' or 'U') || (c > 216 && c < 221) || (c > 248 && c < 253)) return true;
    #Endif; ! TARGET_
    return false;
];

! La fonction enleve_accents enlève les accents de l'input ; de cette manière,
! le joueur peut utiliser les accents ou non. Pour cela le jeu doit définir les
! mots sans accent, par exemple :
!   object set_of_keys "trousseau de clés"
!   with name 'trousseau' 'cles';
! Si le joueur demande : "examiner cles", le mot est compris directement.
! S'il demande : "examiner clés", les accents sont enlevés et on retombe sur le
! cas précédent.

! Note : les ligatures (@oe et @ae) sont considérées comme des accents (vu que
! c'est le même principe : des lettres qui existent mais que tout le monde ne
! va pas utiliser tout le temps, et il faut pouvoir reconnaître les deux formes).

! On peut modifier ce comportement en modifiant la variable "gardeaccents" :
!   1 = l'accent est enlevé uniquement si le mot n'est pas dans le dictionnaire
!       (attention: "clés" dans un name et "cles" dans 10 autres fera un bug sur les 10 autres)
!       Commande de déboguage : accents on
!   2 = les accents ne sont jamais enlevés
!       (pas compatible avec la bibliothèque, puisque les verbes n'ont pas d'accents)
!       Commande de déboguage : accents strict

global gardeaccents = 0;

! Cette fonction transforme le buffer pour enlever les accents et convertir les ligatures
! Elle renvoie 0 si on ne change rien, 1 si on a changé 1 lettre en une autre, 2 si on a
! remplacé la lettre par deux lettres (pour les ligatures)
!
! Note: la conversion en minuscules se fait avant l'appel à cette fonction ; pas besoin de
! tester les versions majuscules des lettres, donc

[ convertir_lettre i ;
    ! on traite les ligatures en premier pour les séparer des a et des o accentués
#IfDef TARGET_ZCODE;
    if (buffer->i == '@ae') { buffer->i='a'; LTI_Insert(++i, 'e'); return 2; }
    if (buffer->i == '@oe') { buffer->i='o'; LTI_Insert(++i, 'e'); return 2; }
#IfNot; ! TARGET_GLULX;
    if (buffer->i == 198 or 230) { buffer->i='a'; LTI_Insert(++i, 'e'); return 2; }
    ! pas d'équivalent pour oe, zut! Le code Unicode étant >255, c'est automatiquement transformé en '?', avant même cette routine...
#Endif;
    ! les autres lettres accentuées
    if (IsAnA(buffer->i)) { buffer->i='a'; return 1; }
    if (IsAnE(buffer->i)) { buffer->i='e'; return 1; }
    if (IsAnI(buffer->i)) { buffer->i='i'; return 1; }
    if (IsAnO(buffer->i)) { buffer->i='o'; return 1; }
    if (IsAnU(buffer->i)) { buffer->i='u'; return 1; }
    switch (buffer->i) {
        'ÿ': buffer->i='y'; return 1;
        'ý': buffer->i='y'; return 1;
        'ñ': buffer->i='n'; return 1;
        'ç': buffer->i='c'; return 1;
    }
    return 0;
];

! Cette fonction enlève les accents et les ligatures dans buffer

[ enleve_accents x i word at len lettresremplacees;
    #Ifdef DEBUG; affiche_buffer(buffer, "  [ enleve_accents :^  - Buffer reçu : "); #Endif;

    if (gardeaccents ~= 2) {
        for (x = 0 : x < NbMots() : x++) { ! pour chaque mot
            word = Mot(x);
            at = PositionMot(x);
            len = LongueurMot(x);
            if (gardeaccents == 0 || (gardeaccents == 1 && word == 0)) {
                #Ifdef DEBUG;
                if (parser_trace >= 7 && gardeaccents == 1) {
                    print "    NON COMPRIS : |";
                    for (i = at : i < at + len : i++) print (char) buffer->i;
                    print "|^";
                }
                #Endif;
                for (i = at : i < at + len : i++) {
                    lettresremplacees = convertir_lettre(i);
                    ! si on a remplacé æ par ae, la longueur a augmenté
                    if (lettresremplacees == 2) { len++; }
                }

                Tokenise__(buffer, parse);
            }
        }
    }

    #Ifdef DEBUG; affiche_buffer(buffer, "  - Buffer sans accents : "); #Endif;
];

! enlève le tiret de départ des mots qui ne sont pas dans le dictionnaire
[ enleve_tirets x i word at len;
    i = NULL; ! pour retirer warning a la compilation glulxe

    #Ifdef DEBUG; affiche_buffer(buffer, "  [ enleve_tirets :^  - Buffer reçu : "); #Endif;

    for (x = 0 : x < NbMots() : x++) { ! pour chaque mot
        word = Mot(x);
        at = PositionMot(x);
        len = LongueurMot(x);
        if (word == 0) { ! non compris
            #Ifdef DEBUG;
            if (parser_trace >= 7) {
                print "    NON COMPRIS : |";
                for (i = at : i < at + len : i++) print (char) buffer->i;
                print "|^";
            }
            #Endif;

            if (buffer->at == '-') buffer->at = ' ';
            Tokenise__(buffer, parse);
        }
    }

    #Ifdef DEBUG; affiche_buffer(buffer, "  - Buffer sans tirets : "); #Endif;
];

[ NbMots; ! nombre de mots dans parse
#Ifdef TARGET_ZCODE;
    return parse->1;
#Ifnot; ! TARGET_GLULX
    return parse-->0;
#Endif; ! TARGET_
];

[ NbChars; ! nombre de chars dans buffer
#Ifdef TARGET_ZCODE;
    return buffer->1;
#Ifnot; ! TARGET_GLULX
    return buffer-->0;
#Endif; ! TARGET_
];

[ Mot n; ! valeur (dictionnaire) du mot numéro n de parse
#Ifdef TARGET_ZCODE;
    return parse-->(n * 2 + 1);
#Ifnot; ! TARGET_GLULX
    return parse-->(n * 3 + 1);
#Endif; ! TARGET_
];

[ PositionMot n; ! position dans buffer du mot numéro n de parse
#Ifdef TARGET_ZCODE;
    return parse->(n * 4 + 5);
#Ifnot; ! TARGET_GLULX
    return parse-->(n * 3 + 3);
#Endif; ! TARGET_
];

[ LongueurMot n;   ! longueur (en chars) dans buffer du mot numéro n de parse
#Ifdef TARGET_ZCODE;
    return parse->(n * 4 + 4);
#Ifnot; ! TARGET_GLULX
    return parse-->(n * 3 + 2);
#Endif; ! TARGET_
];

[ EcraseMot n i;        ! écrase avec des espaces dans buffer le mot numéro n
    for (i = 0 : i < LongueurMot(n) : i++)
        buffer->(PositionMot(n) + i) = ' ';
];

[ DernierMot n; ! vrai si le mot numéro n est le dernier ou suivi d'un "point" (THEN1__WD, ...)
    if (n == NbMots() - 1) ! le mot numéro n est-il le dernier ?
        return true;
    else        ! est-il suivi d'un "point" ?
        return (Mot(n + 1) == THEN1__WD or THEN2__WD or THEN3__WD);
];

[ LanguageToInformese i word wordsuiv at b RangVerbe RangDernier;
    Tokenise__(buffer, parse);
    #Ifdef DEBUG; affiche_buffer(buffer, "[ LanguageToInformese:^* Buffer reçu : "); #Endif;

    ! On enlève les accents de la commande
    !   le seul side-effect de le faire si tôt, c'est "là sortir de" est compris comme "sortir-la de"
    !   on peut penser qu'un joueur qui tape "là sortir de" pour "sortir de là" ne mérite pas qu'on le comprenne
    !                  qu'un joueur qui tape "là sortir de la boîte" mérite qu'on le corrige en "la sortir de la boîte" et qu'on le comprenne
    !   bref, c'est aussi une amélioration
    enleve_accents();

    ! balayer toute la phrase
    for (i = 0 : i < NbMots() : i++) {
        word = Mot(i);
        if (DernierMot(i)) wordsuiv = THEN1__WD; else wordsuiv = Mot(i + 1);
        at = PositionMot(i); ! position du mot numéro i dans buffer
        if (word == 'dessus') {
            LTI_Insert(at, ' '); ! remplace
            buffer->at       = 's';
            buffer->(at + 1) = 'u';
            buffer->(at + 2) = 'r';
            buffer->(at + 3) = '-'; ! contrairement à 'lui', '-lui' peut être féminin !*! mais pas pluriel...
            buffer->(at + 4) = 'l';
            buffer->(at + 5) = 'u';
            buffer->(at + 6) = 'i';
            Tokenise__(buffer, parse);
            i = -1; continue; ! on reprend la passe au début de la phrase
        }
        if (word == 'dessous') {
            LTI_Insert(at, ' '); ! remplace
            buffer->at       = 's';
            buffer->(at + 1) = 'o';
            buffer->(at + 2) = 'u';
            buffer->(at + 3) = 's';
            buffer->(at + 4) = '-'; ! contrairement à 'lui', '-lui' peut être féminin !*! mais pas pluriel...
            buffer->(at + 5) = 'l';
            buffer->(at + 6) = 'u';
            buffer->(at + 7) = 'i';
            Tokenise__(buffer, parse);
            i = -1; continue; ! on reprend la passe au début de la phrase
        }
        ! pas forcément une bonne idée car "dedans" est aussi une direction !*! mais "mets-le dedans" ?
        !if (word == 'dedans') { ! dehors ?
        !    LTI_Insert(at, ' '); ! remplace
        !    LTI_Insert(at, ' ');
        !    buffer->at       = 'd';
        !    buffer->(at + 1) = 'a';
        !    buffer->(at + 2) = 'n';
        !    buffer->(at + 3) = 's';
        !    buffer->(at + 4) = ' ';
        !    buffer->(at + 5) = 'l';
        !    buffer->(at + 6) = 'u';
        !    buffer->(at + 7) = 'y';
        !    Tokenise__(buffer, parse);
        !    i = -1; continue; ! on reprend la passe au début de la phrase
        !}
        ! "nord-est" ou "nord est" devient "nordest"
        if ((word == 'nord-est' or 'nord-ouest') || ((word == 'nord') && (wordsuiv == 'est' or 'ouest'))) {
            buffer->at       = ' '; ! décale
            buffer->(at + 1) = 'n';
            buffer->(at + 2) = 'o';
            buffer->(at + 3) = 'r';
            buffer->(at + 4) = 'd';
            Tokenise__(buffer, parse);
            i = -1; continue; ! on reprend la passe au début de la phrase
        }
        ! "sud-est" ou "sud est" devient "sudest"
        if ((word == 'sud-est' or 'sud-ouest') || ((word == 'sud') && (wordsuiv == 'est' or 'ouest'))) {
            buffer->at       = ' '; ! décale
            buffer->(at + 1) = 's';
            buffer->(at + 2) = 'u';
            buffer->(at + 3) = 'd';
            Tokenise__(buffer, parse);
            i = -1; continue; ! on reprend la passe au début de la phrase
        }
    }

    ! insertion d'un espace avant chaque tiret et après chaque apostrophe
    for (i = WORDSIZE : i < WORDSIZE + NbChars() : i++) {
        if (buffer->i == '-') LTI_Insert(i++, ' ');
        if (buffer->i == ''') LTI_Insert(++i, ' '); ! ''' !*! autre notation ? '\'' par exemple ?
    }
    Tokenise__(buffer, parse);

    ! Suppression de 'je' ou 'j^' en début de phrase.
    ! Par exemple, "je vais au nord" devient "vais au nord".
    if (Mot(0) == 'je' or 'j^') {
        EcraseMot(0);
        Tokenise__(buffer, parse);
    }

    !*! ce qui suit ne tient pas bien compte des commandes s'adressant à quelqu'un ("machin, fais ceci")
    ! Transformation de phrases à l'infinitif commençant par un ou deux pronoms suivis d'un mot (verbe, probablement)
    ! Ex : "le lui donner" devient "donner -le -lui"
    ! Etape A : "le/la/l'/les" (suivi éventuellement de "lui/leur") passe après le verbe. Ex : "lui donner -le"
    word = Mot(0); ! 1er mot
    if ((NbMots() >= 2) && (Mot(1) == 'lui' or 'leur')) RangVerbe = 2; else RangVerbe = 1; ! verbe = 2e ou 3e mot ?
    b = PositionMot(RangVerbe) + LongueurMot(RangVerbe); ! juste après le verbe = position du verbe + longueur du verbe
    if (~~DernierMot(RangVerbe - 1)) { ! ne rien faire si la phrase ne comporte pas de verbe
        if (word == 'le') {
            EcraseMot(0);
            LTI_Insert(b, ' ');
            LTI_Insert(b + 1, '-');
            LTI_Insert(b + 2, 'l');
            LTI_Insert(b + 3, 'e');
        }
        if (word == 'la') {
            EcraseMot(0);
            LTI_Insert(b, ' ');
            LTI_Insert(b + 1, '-');
            LTI_Insert(b + 2, 'l');
            LTI_Insert(b + 3, 'a');
        }
        if (word == 'l^' or 'y//') { ! exemple : "y aller" !*! 'y' à déplacer ?
            EcraseMot(0); !*! imprécision : en fait 'l^' est équivalent à '-le' ou '-la'
            LTI_Insert(b, ' '); ! '-lui' est masculin ou féminin
            LTI_Insert(b + 1, '-');
            LTI_Insert(b + 2, 'l'); !*! 'y' donne '-y' ? et 'en' ?
            LTI_Insert(b + 3, 'u');
            LTI_Insert(b + 4, 'i');
        }
        if (word == 'les') {
            EcraseMot(0);
            LTI_Insert(b, ' ');
            LTI_Insert(b + 1, '-');
            LTI_Insert(b + 2, 'l');
            LTI_Insert(b + 3, 'e');
            LTI_Insert(b + 4, 's');
        }
    }
    Tokenise__(buffer, parse);

    ! Etape B : "lui/leur" passe après le verbe. Ex : "lui donner -le" devient "donner -le -lui"
    word = Mot(0); ! 1er mot
    ! RangDernier est le rang du dernier mot du bloc : verbe + "-le/-la/-les"
    if ((NbMots() >= 3) && (Mot(2) == '-le' or '-la' or '-les' or '-lui')) RangDernier = 2; else RangDernier = 1; ! "-le/-la/-les" après le verbe ?
    b = PositionMot(RangDernier) + LongueurMot(RangDernier); ! juste après bloc = position du dernier + longueur du dernier
    if (~~DernierMot(0)) { ! ne rien faire si la phrase ne comporte pas de verbe
        if (word == 'lui') {
            EcraseMot(0);
            LTI_Insert(b, ' ');
            LTI_Insert(b + 1, '-');
            LTI_Insert(b + 2, 'l');
            LTI_Insert(b + 3, 'u');
            LTI_Insert(b + 4, 'i');
        }
        if (word == 'leur') {
            EcraseMot(0);
            LTI_Insert(b, ' ');
            LTI_Insert(b + 1, '-');
            LTI_Insert(b + 2, 'l');
            LTI_Insert(b + 3, 'e');
            LTI_Insert(b + 4, 'u');
            LTI_Insert(b + 5, 'r');
        }
    }
    Tokenise__(buffer, parse);

    if ((word == 'me' or 'm^' or 'te' or 't^' or 'se' or 's^' or 'nous' or 'vous')
            && ~~DernierMot(0)) { ! sinon gènerait "se", abréviation de "sud-est"
        EcraseMot(0);
        LTI_Insert(b, ' ');
        LTI_Insert(b + 1, 'v');
        LTI_Insert(b + 2, 'o');
        LTI_Insert(b + 3, 'u');
        LTI_Insert(b + 4, 's');
    }
    Tokenise__(buffer, parse);

    ! maintenant que les traitements sur l'infinitif ont été faits,
    ! enlever le tiret en début de mot pour ceux qui n'existent pas dans le dictionnaire
    ! (conserve '-lui','-le','-la'... et les mots prévus par le joueur)
    enleve_tirets();

    ! Avertir ceux qui oublient de mettre des traits d'union entre les pronons et le verbe
    ! à l'impératif et corriger les cas les plus simples : !*! le plus possible
    if (((NbMots() == 2) && (Mot(1) == 'le' or 'la' or 'les' or 'lui' or 'leur')) || ! "parle lui" devient "parle-lui"
            ((NbMots() >= 2) && (Mot(1) == 'lui')) || ! "donne lui la pomme" devient "donne-lui la pomme" (pas de confusion possible avec l'article)
            ((NbMots() >= 4) && (Mot(1) == 'le' or 'la' or 'les' or 'lui' or 'leur') && (Mot(2) == 'a//' or 'au' or 'aux' or 'de' or 'du' or 'des' or 'dans' or 'sur'))) { ! "donne le aux moutons" devient "donne-le aux moutons"
        LTI_Insert(PositionMot(1), '-');
        print "[N'oubliez pas de mettre un trait d'union entre le pronom et le verbe à l'impératif.]^";
    }
    if ((NbMots() == 3) && (Mot(1) == 'le' or 'la' or 'les' or '-le' or '-la' or '-les') && (Mot(2) == 'lui' or 'leur')) { ! "donne le lui" ou "donne-le lui" devient "donne-le-lui"
        if (Mot(1) == 'le' or 'la' or 'les') LTI_Insert(PositionMot(1), '-');
        LTI_Insert(PositionMot(2), '-');
        print "[N'oubliez pas de mettre un trait d'union entre chaque pronom et le verbe à l'impératif.]^";
    }

    ! remplacer "toi/vous/nous" en 2e position par "vous"
    if ((Mot(1) == 'moi' or 'toi' or 'nous')) { !*! pas sûr encore, sert à gérer "réveillons nous" -> "réveillons vous"
        EcraseMot(1); !*! fiable mais le buffer est agrandi de 3 ou 4 caractères : pas grave ?
        LTI_Insert(PositionMot(1)    , 'v');
        LTI_Insert(PositionMot(1) + 1, 'o');
        LTI_Insert(PositionMot(1) + 2, 'u');
        LTI_Insert(PositionMot(1) + 3, 's');
    }

    Tokenise__(buffer, parse);

    #Ifdef DEBUG; affiche_buffer(buffer, "* Buffer traduit en informese : "); #Endif;
];

#Ifdef DEBUG;
[ affiche_buffer buffer msg i len;
    if (parser_trace >= 7) {
#Ifdef TARGET_ZCODE;
        len = buffer->1;
#Ifnot; ! TARGET_GLULX
        len = buffer-->0;
#Endif; ! TARGET_
        print (string) msg, "|";
        for (i = WORDSIZE : i < WORDSIZE + len : i++) print (char) buffer->i;
        print "|^";
    }
];
#Endif;

! ------------------------------------------------------------------------------
!   Part IV.   Printing
! ------------------------------------------------------------------------------

Constant LanguageAnimateGender   = male;
Constant LanguageInanimateGender = male;

! Ces deux fonctions déterminent si un objet est masculin/féminin en prenant en
! compte les genres par défaut déterminés par les constantes ci-dessus.
! L'une n'équivaut pas à la négation de l'autre car les objets peuvent
! théoriquement être neutres (même si l'attribut `neuter` n'est pas utilisé dans
! la traduction française).
! Cela signifie qu'on aurait simplement pu utiliser `obj hasnt female`, mais
! c'est plus robuste avec `GetGNAOfObject`.
! (Voir DM4 p. 247 pour la signification du résultat de `GetGNAOfObject`.)

[ IsMale obj;
    return GetGNAOfObject(obj) == 0 or 3 or 6 or 9;
];

[ IsFemale obj;
    return GetGNAOfObject(obj) == 1 or 4 or 7 or 10;
];

Constant LanguageContractionForms = 2;     ! English has two:
                                           ! 0 = starting with a consonant
                                           ! 1 = starting with a vowel
! Pareil en Français, + h muet pour la forme 1.

[ LanguageContraction text;
    if (IsAnA(text->0) || IsAnE(text->0) || IsAnI(text->0) || IsAnO(text->0) || IsAnU(text->0) || text->0 == 'h' or 'H') return 1;
    return 0;
];

Array LanguageArticles -->

 !   Contraction form 0:     Contraction form 1:
 !   Cdef   Def    Indef     Cdef   Def    Indef

     "Le "  "le "  "un "     "L'"   "l'"   "un "        ! 0: masc sing
     "La "  "la "  "une "    "L'"   "l'"   "une "       ! 1: fem sing
     "Les " "les " "des "    "Les " "les " "des ";      ! 2: plural

                   !             a           i
                   !             s     p     s     p
                   !             m f n m f n m f n m f n

Array LanguageGNAsToArticles --> 0 1 0 2 2 2 0 1 0 2 2 2;

[ LanguageDirection d;
    switch (d) {
        n_to:   print "nord";
        s_to:   print "sud";
        e_to:   print "est";
        w_to:   print "ouest";
        ne_to:  print "nord-est";
        nw_to:  print "nord-ouest";
        se_to:  print "sud-est";
        sw_to:  print "sud-ouest";
        u_to:   print "haut";
        d_to:   print "bas";
        in_to:  print "dedans";
        out_to: print "dehors";
        default: return RunTimeError(9, d);
    }
];

! Inspiré du code trouvé à
! http://www.chambily.com/recursivite/chap_IV_13.htm
! (avec certaines erreurs corrigées).
!
! Quand forbid_s est vrai, c'est qu'on ne peut pas mettre de s à quatre-vingt ou à cent.
! Cela arrive quand on contruit le triplet des milliers : quatre-vingt mille, deux cent mille.
[ LanguageNumber n forbid_s;
    ! Cas spécial pour le zéro.
    if (n == 0) {
        print "zéro";
        rfalse;
    }

    ! Cas spécial du plus petit nombre Glulx.
    ! La raison est que ce nombre et son négatif sont égaux.
    ! On pourrait aussi considérer qu'il s'agit de -0 plutôt que de -2147483648, mais bon.
    ! (Cf. http://inform7.com/mantis/view.php?id=2082)
    #Iftrue (WORDSIZE == 4);
    if (n == -2147483648) {
        print "moins deux milliards ";
        n = 147483648;
    }
    #Endif;

    ! Si négatif, on affiche "moins" puis on continue avec le nombre remis en positif.
    if (n < 0) {
        print "moins ";
        n = -n;
    }

    ! Milliards et millions seulement pour Glulx.
    #Iftrue (WORDSIZE == 4);
    ! Cas des milliards.
    if (n >= 1000000000) {
        print (LanguageNumber) n / 1000000000, " milliard";
        if (n / 1000000000 > 1) {
            print "s";
        }
        n = n % 1000000000;
        if (n) {
            print " ";
        } else {
            return;
        }
    }

    ! Cas des millions.
    if (n >= 1000000) {
        print (LanguageNumber) n / 1000000, " million";
        if (n / 1000000 > 1) {
            print "s";
        }
        n = n % 1000000;
        if (n) {
            print " ";
        } else {
            return;
        }
    }
    #Endif;

    ! Cas des milliers.
    if (n >= 1000) {
        if (n / 1000 ~= 1) { ! Car on ne dit pas "un mille".
            ! On doit interdire le s final pour le triplet des milliers.
            LanguageNumber(n / 1000, true);
            print " ";
        }
        print "mille";
        n = n % 1000;
        if (n) {
            print " ";
        } else {
            return;
        }
    }

    ! Cas des centaines.
    if (n >= 100) {
        if (n / 100 ~= 1) { ! Car on ne dit pas "un cent".
            print (LanguageNumber) n / 100, " ";
        }
        print "cent";
        ! Cas du s de "cent".
        if (n / 100 > 1 && n % 100 == 0 && (~~forbid_s)) {
            print "s";
        }
        n = n % 100;
        if (n) {
            print " ";
        } else {
            return;
        }
    }

    #Iftrue VARIANTE_70 == 0;
    ! Cas spécial du 70.
    if (n >= 70 && n <= 79) {
        print "soixante";
        ! Cas particulier du 71.
        if (n == 71) {
            print " et onze";
            return;
        }
        print "-";
        n = n % 10 + 10;
    }
    #Endif;

    #Iftrue VARIANTE_90 == 0;
    ! Cas spécial du 90.
    if (n >= 90 && n <= 99) {
        print "quatre-vingt-";
        n = n % 10 + 10;
    }
    #Endif;

    ! Les autres cas entre 20 et 99.
    if (n >= 20) {
        switch(n / 10) {
            2: print "vingt";
            3: print "trente";
            4: print "quarante";
            5: print "cinquante";
            6: print "soixante";
            #Iftrue VARIANTE_70 == 1;
            7: print "septante";
            #Endif;
            #Iftrue VARIANTE_80 == 0;
            8: print "quatre-vingt";
            #Endif;
            #Iftrue VARIANTE_80 == 1;
            8: print "huitante";
            #Endif;
            #Iftrue VARIANTE_80 == 2;
            8: print "octante";
            #Endif;
            #Iftrue VARIANTE_90 == 1;
            9: print "nonante";
            #Endif;
        }
        if (n % 10 == 0) {
            #Iftrue VARIANTE_80 == 0;
            ! Cas du s de "quatre-vingts"
            if (n == 80 && (~~forbid_s)) print "s";
            #Endif;
            return;
        }
        #Iftrue VARIANTE_80 == 0;
        ! Exception avec quatre-vingts : on dit quatre-vingt-un et non quatre-vingt et un.
        if (n % 10 == 1 && n ~= 81) {
            print " et ";
        } else {
            print "-";
        }
        #Endif;
        #Iftrue VARIANTE_80 ~= 0;
        if (n % 10 == 1) {
            print " et ";
        } else {
            print "-";
        }
        #Endif;
        n = n % 10;
    }

    ! Cas lorsque on est en dessous de 19.
    if (n <= 19) {
        ! Heureusement, aucun de ces mots de ce tableau ne fait plus de 9 lettres,
        ! sinon ça n'aurait pas marché.
        print (address) LanguageNumbers --> (2*n - 1);
    }
];

[ LanguageTimeOfDay hours mins;
    print hours / 10, hours % 10, "h", mins / 10, mins % 10;
];

! Cette routine est utilisée par PrintCommand, qui affiche la commande tapée
! par le joueur ("Je n'ai compris que : XXX").
[ LanguageVerb i;
    switch (i) {
        'i//', 'inv', 'inventaire':
                print "afficher l'inventaire";
        'r//':  print "regarder";
        'l//':  print "regarder";
        'v//':  print "regarder";
        'x//':  print "examiner";
        'a//':  print "attendre";
        'z//':  print "attendre";
        '!//':  print "dire";
        '?//':  print "demander";
        'q//':  print "quitter";
        'pr':   print "prendre";
        ! Les mots du dictionnaire d'Inform n'ont que 9 lettres au maximum.
        ! Il faut donc indiquer comment afficher un verbe à 10 lettres ou plus.
        'verrouiller':      print "verrouiller";
        'deverrouiller':    print "déverrouiller";
        'introduire':       print "introduire";
        'transferer':       print "transférer";
        'abandonner':       print "abandonner";
        'farfouiller':      print "farfouiller";
        'depoussierer':     print "dépoussierer";
        'questionner':      print "questionner";
        'interroger':       print "interroger";
        ! On n'écrit pas les accents dans un verbe pour plus de facilité pour le joueur (cf enleve_accents).
        ! Il faut donc réécrire les accents pour les verbes qui en ont besoin.
        'inserer':      print "insérer";
        'transferer':   print "transférer";
        'decoller':     print "décoller";
        'lacher':       print "lâcher";
        'oter':         print "ôter";
        'revetir':      print "revêtir";
        'vetir':        print "vêtir";
        'deguiser':     print "déguiser";
        'devorer':      print "dévorer";
        'bruler':       print "brûler";
        'detruire':     print "détruire";
        'ecraser':      print "écraser";
        'elaguer':      print "élaguer";
        'decrire':      print "décrire";
        'ecouter':      print "écouter";
        'gouter':       print "goûter";
        'tater':        print "tâter";
        'trainer':      print "traîner";
        'deplacer':     print "déplacer";
        'regler':       print "régler";
        'devisser':     print "dévisser";
        'eteindre':     print "éteindre";
        'arreter':      print "arrêter";
        'demarrer':     print "démarrer";
        'recurer':      print "récurer";
        'repondre':     print "répondre";
        'reveiller':    print "réveiller";
        'eveiller':     print "éveiller";
        'etreindre':    print "étreindre";
        'reflechir':    print "réfléchir";
        ! Il n'y a pas que les verbes qui sont accentués !
        'a':            print "à";
        'derriere':     print "derrière";
        'cle':          print "clé";
        default: rfalse;
    }
    rtrue;
];

! ----------------------------------------------------------------------------
!  LanguageVerbIsDebugging is called by SearchScope.  It should return true
!  if word w is a debugging verb which needs all objects to be in scope.
! ----------------------------------------------------------------------------

#Ifdef DEBUG;
[ LanguageVerbIsDebugging w;
    if (w == 'purloin' or 'tree' or 'abstract'
                       or 'gonear' or 'scope' or 'showobj')
        rtrue;
    rfalse;
];
#Endif;

! ----------------------------------------------------------------------------
!  LanguageVerbLikesAdverb is called by PrintCommand when printing an UPTO_PE
!  error or an inference message.  Words which are intransitive verbs, i.e.,
!  which require a direction name as an adverb ('walk west'), not a noun
!  ('I only understood you as far as wanting to touch /the/ ground'), should
!  cause the routine to return true.
! ----------------------------------------------------------------------------

[ LanguageVerbLikesAdverb w;
    if (w == 'look' or 'go' or 'push' or 'walk')
        rtrue;
    rfalse;
];

! ----------------------------------------------------------------------------
!  LanguageVerbMayBeName is called by NounDomain when dealing with the
!  player's reply to a "Which do you mean, the short stick or the long
!  stick?" prompt from the parser. If the reply is another verb (for example,
!  LOOK) then then previous ambiguous command is discarded /unless/
!  it is one of these words which could be both a verb /and/ an
!  adjective in a 'name' property.
! ----------------------------------------------------------------------------

[ LanguageVerbMayBeName w;
    if (w == 'long' or 'short' or 'normal'
                    or 'brief' or 'full' or 'verbose')
        rtrue;
    rfalse;
];

Constant NKEY__TX       = "S = suivant";
Constant PKEY__TX       = "P = précédent";
Constant QKEY1__TX      = "        Q = retour"; ! huit espaces pour aligner à droite
Constant QKEY2__TX      = "Q = menu précédent";
Constant RKEY__TX       = "ENTRÉE = lire sujet";

Constant NKEY1__KY      = 'S';
Constant NKEY2__KY      = 's';
Constant PKEY1__KY      = 'P';
Constant PKEY2__KY      = 'p';
Constant QKEY1__KY      = 'Q';
Constant QKEY2__KY      = 'q';

Constant SCORE__TX      = "Score : ";
Constant MOVES__TX      = "Tours : ";
Constant TIME__TX       = "Heure : ";
Constant CANTGO__TX     = "Vous ne pouvez pas aller dans cette direction.";
Constant FORMER__TX     = "votre ancien vous";
Constant YOURSELF__TX   = "vous-même";
Constant YOU__TX        = "Vous";
Constant DARKNESS__TX   = "L'obscurité";

Constant THOSET__TX     = "ces choses-là";
Constant THAT__TX       = "cela";
Constant OR__TX         = " ou ";
Constant NOTHING__TX    = "rien";
! Potentiellement utilisés par WriteListFrom (bit ISARE_BIT), mais pas dans la bibli française
Constant IS__TX         = " se trouve";  ! Constant IS__TX       = "est ";   ! ancienne version
Constant ARE__TX        = " se trouvent";  ! Constant ARE__TX      = "sont ";  ! ancienne version
! Ne pas changer ceux-là, ils sont utilisés dans WriteAfterEntry, qui liste l'inventaire notamment
! (et sont déjà bien faits, cf ListMiscellany plus bas).
Constant IS2__TX        = "";  ! dans/sur lequel " est"  => contenant/supportant
Constant ARE2__TX       = "";  ! dans/sur lequel " sont" => contenant/supportant
Constant AND__TX        = " et ";
Constant WHOM__TX       = "";  ! dans/sur "lequel " est  => contenant/supportant
Constant WHICH__TX      = "";  ! dans/sur "lequel " est  => contenant/supportant
Constant COMMA__TX      = ", ";


! Pas utilisé dans notre traduction.
![ ThatOrThose obj;
!  print "ça";
!];

[ ItOrThem obj; ! ex : avant de pouvoir vous/le/la/les poser... (accusatif ?)
    if (obj == player) { print "vous"; return; }
    if (obj has pluralname)  { print "les"; return; }
    if (IsFemale(obj)) { print "la"; return; }
    else { print "le"; return; }
];

[ IsOrAre obj;
    if (obj has pluralname) print "sont";
    else {
         if (obj == player) print "êtes";
         else print "est";
    }
];

! Ajout pour le français.
[ IsOrAreNot obj;
    if (obj has pluralname) print "ne sont pas";
    else {
         if (obj == player) print "n'êtes pas";
         else print "n'est pas";
    }
];

[ CThatOrThose obj;   ! il/ils/elle/elles semble(nt) ouvert(e) (nominatif)
    ! FIXME: Pourquoi est-ce commenté ?
    !if (obj == player) { print "Vous"; return; }
    if (obj has pluralname) {
        if (IsFemale(obj)) { print "Elles"; return; }
        else { print "Ils"; return; }
    }
    if (IsFemale(obj)) { print "Elle"; return; }
    else { print "Il"; return; }
];

[ CTheyreOrThats obj;
    if (obj == player) { print "Vous êtes"; return; }
    if (obj has pluralname) {
        if (IsFemale(obj)) { print "Elles sont"; return; }
        else { print "Ils sont"; return; }
    }
    if (IsFemale(obj)) { print "Elle est"; return; }
    else { print "Il est"; return; }
];

! ----- Quelques fonctions additionnelles...

! Ajoute `e` et/ou `s` pour l'accord en genre et en nombre.
!   ex: fermé(es), ouvert(es)
! Attention, tous les adjectifs ne peuvent pas utiliser cette logique :
!   ex: pris, prise, pris, prises
[ es obj;
    if (IsFemale(obj)) print "e";
    if (obj has pluralname) print "s";
];

! Ajoute `s` au pluriel.
!   ex: vide(s)
[ s obj;
    if (obj has pluralname) print "s";
];

! Affiche les prépositions "de la" "de l'" "du" "des" selon le contexte.
!   ex: descendre de la voiture / du train
! Important: Comme `(the) obj`, cette fonction affiche le nom de l'objet.
[ DeDuDes obj;
    if (obj == player) { print "de vous"; return; }
    if (obj has pluralname)  { print "des ", (name) obj; return; }
    if (IsFemale(obj) || obj has proper) { print "de ", (the) obj; return; } ! ex : de la voiture, de Paris
    ! FIXME: On ne semble pas gérer le cas masculin + élision (de l'avion).
    ! Cf. implémentation de `to_the` qui le fait.
    ! test ci-dessous à effectuer avec @output_stream (ZCode) et avec PrintAnyToArray (Glulx) ?
    !if ((the) obj = "l'...") { print "de ", (the) obj; return; } ! ex : de l'avion
    print "du ", (name) obj; return; ! ex : du train, du Nautilus (le Nautilus ne doit donc pas être considéré comme proper !)
];

! Affiche les prépositions "à la" "à l'" "au" ou "aux" selon le contexte.
!   ex: aller à la cuisine / au salon
! Important: Comme `(the) obj`, cette fonction affiche le nom de l'objet.
[ to_the obj;
    if (obj has pluralname) {
        print "aux ", (name) obj;
    }
    else if (obj has proper) {
        print "à ", (name) obj;
    }
    else {
        ! Les lignes qui suivent sont copiées de la fonction PrefaceByArticle de parserm.h
        ! afin de connaître la valeur de LanguageContraction pour le nom affiché de l'objet
        ! Je suis le seul à trouver cela atrocement compliqué ?
#ifdef TARGET_ZCODE;
        StorageForShortName-->0 = 160;
        @output_stream 3 StorageForShortName;
        print (PSN__) noun;
        @output_stream -3;
        if (IsMale(obj) && LanguageContraction(StorageForShortName + 2) == 0) {
             print "au ", (name) obj;
        }
        else {
            print "à ", (the) obj;
        }
#ifnot; ! TARGET_GLULX;
        print "à quelqu'un : ", (name) obj;
#endif;
    }
];

! Ajoute le suffixe "nt" pour le pluriel des verbes du 1er groupe.
!   ex: semble(nt)
! Attention, ça ne marche pas pour les autres groupes :
!   ex: paraît, paraissent
[ nt obj;
    if (obj has pluralname) print "nt";
];

! ----- Messages par défaut pour les actions

[ LanguageLM n x1;
    Answer,Ask: "Pas de réponse.";
!   Ask:        see Answer
    Attack:     "La violence n'est pas une solution ici.";
    Blow:       "Vous ne pouvez pas utilement souffler dedans.";
    Burn:       "Cet acte dangereux ne mènerait pas à grand-chose.";
    Buy:        print_ret (The) x1, " ", (IsOrAreNot) x1, " à vendre.";
    Climb:      "Escalader ceci n'accomplirait pas grand chose."; ! basé sur la lib EN 6.12
    Close: switch (n) {
            1:  "Vous ne pouvez pas fermer cela.";
            2:  print_ret (CTheyreOrThats) x1, " déjà fermé", (es) x1, ".";
            3:  "Vous fermez ", (the) x1, ".";
    }
    CommandsOff: switch (n) {
            1:  "[Enregistrement des commandes désactivé.]";
            #Ifdef TARGET_GLULX;
            2:  "[Enregistrement des commandes déjà désactivé.]";
            #Endif; ! TARGET_
    }
    CommandsOn: switch (n) {
            1:  "[Enregistrement des commandes activé.]";
            #Ifdef TARGET_GLULX;
            2:  "[Les commandes sont actuellement en train d'être rejouées.]";
            3:  "[Enregistrement des commandes déjà activé.]";
            4:  "[Échec d'enregistrement des commandes.]";
            #Endif; ! TARGET_
    }
    CommandsRead: switch (n) {
            1:  "[Rejouer les commandes.]";
            #Ifdef TARGET_GLULX;
            2:  "[Les commandes sont déjà en train d'être rejouées.]";
            3:  "[Rejouer les commandes a échoué. L'enregistrement des commandes est activé.]";
            4:  "[Rejouer les commandes a échoué.]";
            5:  "[Rejouer les commandes achevé.]";
            #Endif; ! TARGET_
    }
    Consult:    ! Ce premier test ne semble pas utilisable parce que notre grammaire
                ! renvoie `creature` vers Ask.
                if (x1 has animate || x1 has talkable) {
                    print (The) x1, " ", (IsOrAreNot) x1, " consultable", (s) x1, " de cette manière.^";
                }
                else {
                    print "Vous ne découvrez rien. Soit ", (the) x1, " ";
                    print (IsOrAreNot) x1, " consultable", (s) x1;
                    " ainsi, soit vous tentez de consulter sur le mauvais sujet.";
                }
    CrierSansPrecision:
                "Cela ne mènerait à rien.";
    Cut:        "Couper ", (the) x1, " ne mènerait pas à grand-chose.";
    Dig:        if (noun == 0) "Creuser ne mènerait à rien ici.";
                else "Creuser cela ne mènerait à rien.";
    Disrobe: switch (n) {
            1:  "Vous ne ", (ItorThem) x1, " portez pas sur vous.";
            2:  "Vous enlevez ", (the) x1, ".";
    }
    Drink:      "Ceci n'a rien de buvable.";
    Drop: switch (n) {
            1:  print_ret (The) x1, " ", (IsOrAre) x1, " déjà là.";
            2:  "Vous n'avez pas cela sur vous.";
            3:  "(vous enlevez d'abord ", (the) x1, ")"; ! worn -> Disrobe
            4:  "D'accord.";
    }
    Eat: switch (n) {
            1:  print_ret (The) x1, " ", (IsOrAre) x1,
                " non comestible", (s) x1, ", selon toute évidence.";
            2:  "Vous mangez ", (the) x1, ". Pas mauvais.";
    }
    EmptyT: switch (n) {
            1:  if (x1 == player) print_ret "En voilà une idée.";
                print_ret (The) x1, " ", (IsOrAreNot) x1, " un récipient que l'on peut vider.";
            2:  print_ret (The) x1, " ", (IsOrAre) x1, " fermé", (es) x1, ".";
            3:  print_ret (The) x1, " ", (IsOrAre) x1, " déjà vide", (s) x1, ".";
            4:  "Ceci ne viderait rien.";
    }
    Enter: switch (n) {
            1:  print "Vous êtes déjà ";
                if (x1 has supporter) print "sur "; else print "dans ";
                print_ret (the) x1, ".";
            2:  print "Vous ne pouvez pas ";
                switch (verb_word) {
                    'entrer': "y entrer.";
                    'assoir': "vous y asseoir.";
                    'asseoir': "vous y asseoir.";
                    'allonger': "vous y allonger.";
                    'coucher': "vous y coucher.";
                    'monter': "y monter.";
                    default: "y aller."; ! plutôt que "y entrer."
                }
            3:  "Vous ne pouvez entrer dans ", (the) x1, " fermé", (es) x1, ".";
            4:  print "Vous ne pouvez pas ";
                if (x1 has supporter) print "y monter";
                else print "y entrer";
                " si ce n'est pas posé."; ! sur pieds, non encastré (freestanding)
            5:  print "Vous ";
                if (x1 has supporter) print "montez sur "; else print "entrez dans ";
                print_ret (the) x1, ".";
            6:  print "(";
                if (x1 has supporter) print "descendant "; else print "sortant ";
                print (DeDuDes) x1; ")";
            7:  if (x1 has supporter) "(montant sur ", (the) x1, ")^";
                if (x1 has container) "(entrant dans ", (the) x1, ")^";
                "(entrant dans ", (the) x1, ")^";
    }
    Examine: switch (n) {
            1:  "Vous ne pouvez rien voir.";
            2:  "Rien de particulier concernant ", (the) x1, ".";
            3:  print (The) x1, " ", (IsOrAre) x1, " actuellement ";
                if (x1 has on) "allumé", (es) x1, "."; else "éteint", (es) x1, ".";
    }
    Exit: switch (n) {
            1:  "Vous n'êtes à l'intérieur de rien pour le moment.";
            2:  print "Vous ne pouvez pas sortir ";
                print_ret (DeDuDes) x1, " fermé", (es) x1, ".";
            3:  print "Vous ";
                if (x1 has supporter) print "descendez "; else print "sortez ";
                print_ret (DeDuDes) x1, ".";
            4:  print "Vous n'êtes pas ";
                if (x1 has supporter) print "sur "; else print "dans ";
                print_ret (the) x1, ".";
    }
    Fill:       "Cela ne vous avancerait pas.";
    FullScore: switch (n) {
            1:  if (deadflag) print "Le score était ";
                else          print "Le score est ";
                "composé comme suit :^";
            2:  "trouver divers objets";
            3:  "visiter différents endroits";
            4:  print "total (sur ", MAX_SCORE; ")";
    }
    GetOff:     "Vous n'êtes pas sur ", (the) x1, " en ce moment.";
    Give: switch (n) {
            1:  "Vous n'avez pas en main ", (the) x1, ".";
            2:  "Vous vous remerciez pour ce cadeau.";
            3:  print (The) x1;
                if (x1 has pluralname) print " n'ont pas"; else print " n'a pas";
                " l'air intéressé.";
    }
    Go: switch (n) {
            1:  print "Vous devez d'abord ";
                if (x1 has supporter) print "descendre "; else print "sortir ";
                print_ret (DeDuDes) x1, ".";
            2:  "Vous ne pouvez pas aller par là.";
            3:  "Vous êtes incapable de gravir ", (the) x1, ".";
            4:  "Vous êtes incapable de descendre par ", (the) x1, ".";
            5:  "Vous ne pouvez pas, puisque ", (the) x1, " ", (IsOrAre) x1,
                " sur votre chemin.";
            6:  print "Vous ne pouvez pas, puisque ", (the) x1;
                print_ret " ne mène", (nt) x1, " nulle part.";
    }
    Insert: switch (n) {
            1:  "Vous devez avoir en main ", (the) x1,
                " avant de pouvoir ", (ItorThem) x1,
                " mettre dans autre chose.";
            2:  if (x1 has pluralname) print (The) x1, " ne peuvent pas";
                else print (The) x1, " ne peut pas";
                " contenir d'objet.";
            3:  print_ret (The) x1, " ", (IsOrAre) x1, " fermé", (es) x1, ".";
            4:  "Vous avez besoin de ", (ItorThem) x1, " prendre d'abord.";
            5:  "Vous ne pouvez pas mettre un objet à l'intérieur de lui-même.";
            ! On pourrait dire "enlever", mais cela obligerait à gérer le cas "l'"
            6:  "(vous ", (ItorThem) x1, " retirez d'abord)^"; ! worn -> Disrobe
            7:  "Il n'y a plus de place dans ", (the) x1, ".";
            8:  "D'accord.";
            9:  "Vous mettez ", (the) x1, " dans ", (the) second, ".";
    }
    Inv: switch (n) {
            1:  "Vous n'avez rien.";
            2:  print "Vous avez";
            3:  print " :^";
            4:  print ".^";
    }
    Jump:       "Sauter sur place ne vous avancerait en rien.";
    JumpOver:   "Vous n'arriveriez à rien en faisant cela.";
    Kiss:       "Concentrez-vous sur le jeu.";
    LeverNonGere:
                "[Ce jeu ne comprend le verbe @<< lever @>> que dans la formule @<< se lever @>>]";
    Listen:     "Vous n'entendez rien d'inattendu.";
    ListMiscellany: switch (n) {
            ! Ici, "allumé" traduit "providing light". Cela fonctionne pour les lampes,
            ! mais sera un peu moins approprié pour un objet luminescent.
            ! Faudrait-il plutôt opter pour une formule telle que "source lumineuse" ?
            1:  print " (allumé", (es) x1, ")";
            2:  print " (qui ", (IsOrAre) x1, " fermé", (es) x1, ")";
            3:  print " (fermé", (es) x1, " et allumé", (es) x1, ")";
            4:  print " (qui ", (IsOrAre) x1, " vide", (s) x1, ")";
            5:  print " (vide", (s) x1, " et allumé", (es) x1, ")";
            6:  print " (qui ", (IsOrAre) x1, " fermé", (es) x1, " et vide", (s) x1, ")";
            7:  print " (fermé", (es) x1, ", vide", (s) x1, " et allumé", (es) x1, ")";
            8:  print " (allumé", (es) x1, " et porté", (es) x1;
            9:  print " (allumé", (es) x1;
            10: print " (porté", (es) x1;
            11: print " (qui ", (IsOrAre) x1, " ";
            12: print "ouvert", (es) x1;
            13: print "ouvert", (es) x1, " mais vide", (s) x1;
            14: print "fermé", (es) x1;
            15: print "fermé", (es) x1, " et verrouillé", (es) x1;
            16: print " et vide", (s) x1;
            17: print " (qui ", (IsOrAre) x1, " vide", (s) x1, ")";
            18: print " contenant ";
            ! Cela semble inversé par rapport à la lib EN, mais en réalité c'est correct,
            ! car en anglais on aurait par exemple "(on which are..."), qui chez nous devient
            ! "(supportant...)", car on définit IS2__TX, ARE2__TX, WHOM__TX, WHICH__TX à ""
            ! Cela dit, "Vous pouvez voir un arbre (sur lequel est une corde)" serait un poil
            ! plus beau que l'actuel "Vous pouvez voir un arbre (supportant une corde)".
            19: print " (supportant "; ! " (sur ";
            20: print " supportant ";  ! " sur ";
            21: print " (contenant ";  ! " (dans ";
            22: print " contenant ";   ! " dans ";
    }
    LMode1:     " est passé en mode description normale ; seuls les lieux
                  visités pour la première fois sont décrits en détail.";
    LMode2:     " est passé en mode description longue ; tous les lieux
                  même déjà visités, sont décrits en détail.";
    LMode3:     " est passé en mode description courte ; tous les lieux,
                  même non visités, sont décrits brièvement.";
    Lock: switch (n) {
            1:  "Vous ne pouvez pas verrouiller cela.";
            2:  print_ret (CTheyreOrThats) x1, " verrouillé en ce moment.";
            3:  "Vous devrez fermer ", (the) x1, " d'abord.";
            4:  "Cela ne rentre pas dans la serrure.";
            5:  "Vous verrouillez ", (the) x1, ".";
    }
    Look: switch (n) {
            1:  print " (sur ", (the) x1, ")";
            2:  print " (dans ", (the) x1, ")";
            3:  print " (comme ", (object) x1, ")";
            4:  print "^Sur ", (the) x1, ", ";
                WriteListFrom(child(x1),
                    ENGLISH_BIT + RECURSE_BIT + PARTINV_BIT + TERSE_BIT + CONCEAL_BIT);
                ".";
            5,6:
                if (x1 ~= location) {
                    if (x1 has supporter) print "^Sur "; else print "^Dans ";
                    print (the) x1, " vous";
                }
                else print "^Vous";
                print " pouvez voir ";
                if (n == 5) print "aussi ";
                WriteListFrom(child(x1),
                    ENGLISH_BIT + RECURSE_BIT + PARTINV_BIT + TERSE_BIT + CONCEAL_BIT + WORKFLAG_BIT);
                ".";
            7:  "Vous ne voyez rien de particulier dans cette direction.";
    }
    LookUnder: switch (n) {
            1:  "Vous n'y voyez rien dans le noir.";
            2:  "Votre action ne produit aucune découverte notable.";
    }
    Mild:       "Eh oui.";
    Miscellany: switch (n) {
            1:  "(affichage des seize premiers)^";
            2:  "Il n'y a rien à faire.";
            3:  print " Vous avez perdu ";
            4:  print " Vous avez gagné ";
            5:  print "^Voulez-vous RECOMMENCER, CHARGER une partie sauvegardée";
                #Ifdef DEATH_MENTION_UNDO;
                print ", ANNULER votre dernière action";
                #Endif;
                if (TASKS_PROVIDED == 0)
                    print ", obtenir le score détaillé pour cette partie (FULLSCORE)";
                if (deadflag == 2 && AMUSING_PROVIDED == 0)
                    print ", lire quelques suggestions amusantes (AMUSING)";
                " ou QUITTER ?";
            6:  "[Votre interpréteur ne permet pas d'@<< annuler @>>. Désolé !]";
            #Ifdef TARGET_ZCODE;
            7:  "[@<< Annuler @>> a échoué. Tous les interpréteurs ne le permettent pas.]";
            #Ifnot; ! TARGET_GLULX
            7:  "[Vous ne pouvez pas @<< annuler @>> plus que cela.]";
            #Endif; ! TARGET_
            8:  "Faites un choix parmi les propositions ci-dessus.";
            9:  "^Vous êtes à présent dans le noir total.";
            10: "[Je vous demande pardon ?]";
            11: "[Vous ne pouvez pas @<< annuler @>> alors que rien n'a encore été fait.]";
            12: "[Impossible d'@<< annuler @>> deux fois de suite. Désolé !]";
            13: "[Action précédente annulée.]";
            14: "[Désolé, impossible de corriger.]";
            15: "N'y pensez même pas.";
            16: "[@<< Oups @>> ne peut corriger qu'un seul mot.]";
            17: "Il fait noir, et vous ne pouvez rien voir.";
            18: print "vous-même";
            19: "Votre apparence est aussi agréable qu'à l'accoutumée.";
            20: "[Pour répéter une commande comme @<< grenouille, saute @>>, dites seulement
                @<< encore @>>, et pas @<< grenouille, encore @>>.]";
            21: "[Vous pouvez difficilement répéter cela.]";
            22: "[Vous ne pouvez pas commencer par une virgule.]";
            23: "[Vous semblez vouloir parler à quelqu'un, mais je ne vois pas à qui.]";
            24: "Vous ne pouvez pas discuter avec ", (the) x1, ".";
                ! "parler à" serait mieux mais délicat (ex: à l'oiseau)
            25: "[Pour parler à quelqu'un, essayez @<< quelqu'un, bonjour @>> ou quelque chose dans le genre.]";
            26: "(vous prenez d'abord ", (the) not_holding, ")";
            27: "[Je ne comprends pas cette phrase.]";
            28: print "[Merci de reformuler. Je n'ai compris que : ";
            29: "[Je n'ai pas compris ce nombre.]";
            30: "Vous ne voyez rien de tel, à moins que cela ne soit sans grande importance.";
            31: "[Vous semblez en avoir dit trop peu.]";
            32: "Vous ne l'avez pas dans vos mains.";
            33: "[Vous ne pouvez pas utiliser plusieurs objets à la fois avec ce verbe.]";
            34: "[Vous ne pouvez utiliser plusieurs objets à la fois qu'une fois par ligne.]";
            35: "[Je ne suis pas sûr de ce à quoi @<< ", (address) pronoun_word,
                " @>> se réfère.]";
            36: "[Vous avez exclu quelque chose qui n'était de toute façon pas compris dans la liste.]";
            37: "[Vous ne pouvez agir ainsi qu'avec une chose animée.]";
            38: "[Je ne reconnais pas ce verbe.]";
            39: "[Ce n'est pas une chose à laquelle vous aurez à vous référer au cours du jeu.]";
            40: "Vous ne pouvez voir @<< ", (address) pronoun_word,
                " @>> (", (the) pronoun_obj, ") pour l'instant.";
            41: "[Je n'ai pas compris la fin de la phrase.]";
            ! Ne faudrait-il pas accorder "Aucun" en genre ?
            42: if (x1 == 0) print "Aucun de disponible.";
                else print "Il y en a seulement ", (number) x1, ".";
            43: "Il n'y a rien à faire.";
            44: "Rien n'est disponible.";
            45: print "[Précisez : ";
            46: print "[Précisez : ";
            47: "[Désolé, vous pouvez seulement avoir un objet ici. Lequel voulez-vous exactement ?]";
            48: print "[Précisez qui est concerné par cette action.]^";
            49: print "[Précisez l'objet concerné par cette action, ou à utiliser.]^";
            50: print "Votre score vient ";
                if (x1 > 0) print "d'augmenter"; else { x1 = -x1; print "de diminuer"; }
                print " de ", (number) x1, " point";
                if (x1 > 1) print "s";
            ! "something dramatic" traduit par "quelque chose d'important"
            51: "(Comme quelque chose d'important vient de se produire,
                votre liste de commandes a été raccourcie.)";
            52: "^Tapez un nombre entre 1 et ", x1,
                ", 0 pour réafficher ou appuyez sur ENTRÉE.^";
            53: "^[Appuyez sur ESPACE.]"; !*! SVP
            54: "[Commentaire enregistré.]";
            55: "[Commentaire NON enregistré.]";
            56: print ".]^";  ! L__M 28
            57: print " ?]^"; ! L__M 45 et 46
    }
    No,Yes:     "C'était une question rhétorique.";
    NotifyOff:  "[Notification du score désactivée.]";
    NotifyOn:   "[Notification du score activée.]";
    Objects: switch (n) {
            ! La lib EN va plus loin : "Untel a manipulé :", en tenant compte de actor.
            1:  "Objets ayant été manipulés :^";
            ! Et ici cela devrait dire : "Untel n'a rien manipulé"
            2:  "Aucun.";
            3:  print "   (sur le corps)";
            4:  print "   (dans l'inventaire)";
            5:  print "   (donné", (es) x1, ")";
            6:  print "   (", (name) x1, ")"; ! visited
            7:  print "   (dans ", (the) x1, ")"; ! enterable
            8:  print "   (dans ", (the) x1, ")"; ! container
            9:  print "   (sur ", (the) x1, ")"; ! supporter
            ! Dans le message 10, x1 est détruit, donc ne pas tenter de l'utiliser pour l'accord
            10: print "   (n'existe plus)";
    }
    Open: switch (n) {
            1:  print_ret "Vous ne pouvez pas ouvrir ", (the) x1, ".";
            ! "verrouillé" serait plus passe-partout que "fermé à la clé" ?
            2:  print_ret (CThatOrThose) x1, " semble", (nt) x1,
                " être fermé", (es) x1, " à clé.";
            3:  print_ret (CTheyreOrThats) x1, " déjà ouvert", (es) x1, ".";
            4:  print "Vous ouvrez ", (the) x1, ", révélant ";
                if (WriteListFrom(child(x1),
                    ENGLISH_BIT + TERSE_BIT + CONCEAL_BIT) == 0) "rien du tout.";
                ".";
            5:  "Vous ouvrez ", (the) x1, ".";
    }
    Order:      print (The) x1;
                if (x1 has pluralname) print " ont";
                else print " a";
                " mieux à faire.";
    ParlerIncorrect:
                "[Veuillez préciser ou reformuler votre pensée.]";
    ParlerSansPrecision:
                if (x1 == player) "Vous ne savez pas quoi vous dire que vous ne sachiez déjà.";
                else "Pas de réponse.";
    Places: switch (n) {
            1:  print "Vous avez visité : ";
            2:  print ".^";
    }
    Pray:       "Rien de concret ne résulte de votre prière.";
    Prompt:     print "^>";
    Pronouns: switch (n) {
            1:  print "En ce moment, ";
            2:  print "signifie ";
            3:  print "n'est pas défini";
            4:  "aucun pronom n'est connu du jeu.";
            5:  ".";
    }
    Pull,Push,Turn: switch (n) {
            ! "It's fixed in place" adapté en "C'est fixe."
            ! La logique ici est de considérer que "fixed in place", en anglais, n'implique pas
            ! que quelqu'un a fait exprès de fixer la chose, or "fixé", en français, me semble
            ! le suggérer (Stormi). On évite cet écueil en traduisant par "C'est fixe".
            ! Autre possibilité, qui s'éloigne de l'anglais mais répondrait un peu mieux à la volonté
            ! d'action des joueurs/joueuses : "Cela reste immobile".
            ! Ancienne traduction : "C'est fixé sur place."
            1:  "C'est fixe.";
            2:  "Vous en êtes incapable.";
            3:  "Rien d'évident ne se produit.";
            4:  "Cela serait moins que courtois.";
    }
!   Push:       see Pull
    PushDir: switch (n) {
            1:  "Vous ne pouvez donc rien imaginer de mieux ?";
            2:  "[Ce n'est pas une direction.]";
            3:  "Vous ne pouvez pas dans cette direction.";
    }
    PutOn: switch (n) {
            1:  "Vous devez avoir en main ", (the) x1,
                " avant de pouvoir ", (ItorThem) x1,
                " mettre sur quelque chose d'autre.";
            2:  "Vous ne pouvez pas poser un objet sur lui-même.";
            3:  "Poser des objets sur ", (the) x1, " ne mènerait à rien.";
            4:  "Vous manquez de dextérité.";
            ! On pourrait dire "enlever", mais cela obligerait à gérer le cas "l'"
            5:  "(vous ", (ItorThem) x1, " retirez d'abord)^"; ! worn -> Disrobe
            6:  "Il n'y a plus assez de place sur ", (the) x1, ".";
            7:  "C'est fait.";
            8:  "Vous mettez ", (the) x1, " sur ", (the) second, ".";
    }
    Quit: switch (n) {
            1:  print "Répondez par oui ou par non, je vous prie.";
            2:  print "Voulez-vous vraiment quitter ? (O/N) ";
    }
    Remove: switch (n) {
            1:  print_ret (CTheyreOrThats) x1, " malheureusement fermé", (es) x1, ".";
            2:  if (x1 has pluralname) {
                    if (IsFemale(x1)) { print "Elles"; } else { print "Ils"; }
                    " n'y sont pas.";
                } else {
                    "Ça n'y est pas.";
                }
            3:  "D'accord.";
    }
    Restart: switch (n) {
            1:  print "Voulez-vous vraiment recommencer ? (O/N) ";
            2:  "Échec.";
    }
    Restore: switch (n) {
            1:  "[Échec du chargement.]";
            2:  "[La partie a bien été restaurée.]^"; ! lib EN : "Ok."
    }
    Rub:        if (x1 has animate) "Tenez donc vos mains tranquilles."; ! pas dans la lib EN 6.11
                else "Vous n'arrivez à rien ainsi.";
    Save: switch (n) {
            1:  "[La sauvegarde a échoué.]";
            2:  "[La partie a bien été sauvegardée.]"; ! lib EN : "Ok."
    }
    Score: switch (n) {
            1:  if (deadflag) print "Dans cette partie vous avez obtenu ";
                else print "Vous avez jusqu'à présent obtenu ";
                print score, " sur un score possible de ", MAX_SCORE, ", en ", turns, " tour";
                if (turns ~= 1) print "s";
                return;
            2:  "Il n'y a pas de score dans cette histoire.";
    }
    ScriptOff: switch (n) {
            1:  "[Aucune transcription en cours.]";
            2:  "^[Fin de transcription.]";
            3:  "[Impossible de terminer la transcription.]";
    }
    ScriptOn: switch (n) {
            1:  "[Transcription déjà en cours.]";
            2:  "Début d'une transcription de...";
            3:  "[Impossible de commencer la transcription.]";
    }
    Search: switch (n) {
            1:  "Vous n'y voyez rien dans le noir.";
            2:  "Il n'y a rien sur ", (the) x1, ".";
            3:  print "Sur ", (the) x1, ", vous voyez ";
                WriteListFrom(child(x1),
                    ENGLISH_BIT + TERSE_BIT + CONCEAL_BIT);
                ".";
            4:  if (x1 has animate) "Tenez donc vos mains tranquilles."; ! pas dans la lib EN 6.11
                else "Vous ne trouvez rien d'intéressant.";
            5:  "Vous ne pouvez pas voir à l'intérieur, puisque ", (the) x1, " ",
                (IsOrAre) x1, " fermé", (es) x1, ".";
            6:  print_ret (The) x1, " ", (IsOrAre) x1, " vide", (s) x1, ".";
            7:  print (The) x1;
                if (x1 has pluralname) print " contiennent ";
                else print " contient ";
                WriteListFrom(child(x1),
                    ENGLISH_BIT + TERSE_BIT + CONCEAL_BIT);
                ".";
    }
    SeLever:    "Inutile.";
    Set:        "Non, vous ne pouvez pas ", (ItorThem) x1, " régler.";
    SetTo:      "Non, vous ne pouvez ", (ItorThem) x1, " régler sur rien.";
    Show: switch (n) {
            1:  "Vous n'avez pas ", (the) x1, ".";
            2:  print (The) x1;
                if (x1 has pluralname) print " n'ont pas";
                else print " n'a pas";
                " l'air intéressé."; ! lib EN 6.11 "unimpressed"
    }
    Sing:       "Vous ne chantez pas si bien que ça, alors vous n'osez pas."; ! lib EN 6.11 "Your singing is abominable"
    Sleep:      "Vous n'avez pas particulièrement sommeil.";
    Smell:      "Vous ne sentez rien d'inattendu.";
    Sorry:      "Ce n'est pas grave."; ! lib EN 6.11 "Oh, don't apologise"
    Squeeze: switch (n) {
            1:  "Surveillez vos mains.";
            2:  "Vous n'arriverez à rien ainsi."; ! lib EN 6.11 "Vous achieve nothing by this."
    }
    Strong:     "Vous n'avez pas besoin de ce genre de langage dans cette aventure.";
    Swim:       "Il n'y a pas assez d'eau pour nager.";
    ! Lib EN 6.11: "There's nothing sensible to swing here.";
    ! J'ai l'impression que faire se balancer quelque chose et se balancer soi-même avec
    ! ne sont pas distingués, et c'est galère à gérer en Français.
    ! Une certaine proximité avec "Wave", aussi, à la différence que Wave impose un objet tenu.
    Swing:      "Ce n'est pas une chose à laquelle il est utile de se balancer.";
    SwitchOff: switch (n) {
            1:  "Vous ne pouvez pas allumer ou éteindre cela.";
            2:  print_ret (CTheyreOrThats) x1,
                " déjà éteint", (es) x1, ".";
            3:  "Vous éteignez ", (the) x1, ".";
    }
    SwitchOn: switch (n) {
            1:  "Vous ne pouvez pas allumer cela.";
            2:  print_ret (CTheyreOrThats) x1,
                " déjà allumé", (es) x1, ".";
            3:  "Vous allumez ", (the) x1, ".";
    }
    Take: switch (n) {
            1:  "D'accord.";
            2:  "On ne peut pas se prendre soi-même.";
            3:  print "Je ne pense pas qu'";
                if (IsFemale(x1)) { print "elle"; } else { print "il"; }
                if (x1 has pluralname) { print "s apprécieraient"; } else { print " apprécierait"; }
                ".";
            4:  print "Il faudrait d'abord "; ! Lib EN 6.11: "You'd have to..."
                if (x1 has supporter) print "descendre "; else print "sortir ";
                print_ret (DeDuDes) x1, ".";
            5:  "Vous l'avez déjà.";
            6:  "Cela semble appartenir ", (to_the) x1, ".";
            7:  "Cela semble faire partie ", (DeDuDes) x1, ".";
            8:  print_ret (CThatOrThose) x1, " ", (IsOrAreNot) x1,
                " disponible", (s) x1, ".";
            9:  print_ret (The) x1, " ", (IsOrAreNot) x1, " ouvert", (es) x1, ".";
            10: "C'est trop difficile à transporter.";
            11: "C'est fixe."; ! voir Pull, Push et Turn
            12: "Vous transportez déjà trop de choses.";
            13: "(vous mettez ", (the) x1, " dans ", (the) SACK_OBJECT,
                " pour faire de la place)";
    }
    Taste:      if (x1 has animate) "Cela ne serait pas très convenable.";
                ! lib EN 6.11: "You taste nothing unexpected"
                else "Vous préférez ne pas goûter n'importe quoi. On ne sait jamais où ça a pu traîner.";
    Tell: switch (n) {
            1:  "Vous discutez avec vous-même pendant un bon moment...";
            2:  "Pas de réaction.";
    }
    Think:      "Vous réfléchissez un peu, mais aucune idée ne vous vient à l'esprit."; ! lib EN "What a good idea."
    ThrowAt: switch (n) {
            1:  "Futile.";
            2:  "Vous hésitez au moment crucial.";
    }
    Tie:        "Vous n'arriveriez à rien en faisant cela.";
    Touch: switch (n) {
            1:  "Tenez vos mains tranquilles.";
            2:  "Vous préférez ne pas mettre vos mains n'importe où."; ! Lib EN 6.11 "You feel nothing unexpected"
            3:  "Si vous pensez que ça peut aider.";
    }
!   Turn:       see Pull.
    Unlock: switch (n) {
            1:  "Vous ne pouvez pas déverrouiller cela.";
            2:  print_ret (CTheyreOrThats) x1, " déjà déverrouillé", (es) x1, ".";
            3:  "Cela ne rentre pas dans la serrure.";
            4:  "Vous déverrouillez ", (the) x1, ".";
    }
    VagueConsult:
                "[Précisez sur quel sujet vous souhaitez consulter ", (the) x1, ".]";
    VagueDo:    "[Je ne connais qu'un nombre limité d'expressions avec @<< faire @>>. Utilisez peut-être un autre verbe ?]";
    VagueGo:    "[Vous devez donner la direction dans laquelle aller.]";
    VagueSearch:
                "[Utilisez plutôt @<< fouiller @>>.]";
    VagueTie:   "[Reformulez : ", (address) verb_word, " ... avec ...]";
    VagueUse:   "[Veuillez indiquer un verbe plus précis.]";
    Verify: switch (n) {
            1:  "[Le fichier semble intact.]";
            2:  "[Le fichier est certainement corrompu !]";
    }
    Wait:       "Le temps passe...";
    Wake:       "Il ne s'agit pas d'un rêve.";
    WakeOther:  "Cela ne semble pas nécessaire.";
    Wave: switch (n) {
            1:  "Vous n'avez pas cela.";
            ! Lib EN 6.11 "You look ridiculous waving ", (the) x1, ".";
            2:  "Vous auriez l'air ridicule en agitant ", (the) x1, ".";
    }
    WaveHands:  if (x1 == 0 || x1 == player) "Vous auriez l'air bête en agitant vos mains devant vous.";
                else "Votre timidité prend le dessus au dernier moment.";
    Wear: switch (n) {
            1:  "Vous ne pouvez pas porter cela.";
            2:  "Vous n'avez pas cela.";
            3:  "Vous ", (ItorThem) x1, " portez déjà sur vous.";
            4:  "Vous mettez ", (the) x1, ".";
    }
!   Yes:        see No.
];

! Affichage des pronoms - avec les guillemets français
[ PronomsSub x y c d;
    L__M(##Pronouns, 1);

    c = (LanguagePronouns-->0) / 3;
    if (player ~= selfobj) c++;

    if (c == 0) return L__M(##Pronouns, 4);

    for (x = 1, d = 0 : x <= LanguagePronouns-->0 : x = x + 3) {
        print "@<< ", (address) LanguagePronouns-->x, " @>> ";
        y = LanguagePronouns-->(x + 2);
        if (y == NULL) L__M(##Pronouns, 3);
        else {
            L__M(##Pronouns, 2);
            print (the) y;
        }
        d++;
        if (d < c - 1) print (string) COMMA__TX;
        if (d == c - 1) print (string) AND__TX;
    }
    if (player ~= selfobj) {
        print "@<< ", (address) ME1__WD, " @>> "; L__M(##Pronouns, 2);
        c = player; player = selfobj;
        print (the) c; player = c;
    }
    L__M(##Pronouns, 5);
];

! ==============================================================================

Constant LIBRARY_FRENCH;       ! for dependency checking.

! ==============================================================================
